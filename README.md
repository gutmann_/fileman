# README #

### Virtual file manager ###

* This project demonstrate some principles of working with catalog tree (in code).
* Version 0.2

### How do I get set up? ###

* You need UNIX-based system (I'm not tested it on something different than Gnu/Linux)
* You need depot_tools (see https://www.chromium.org/developers/how-tos/install-depot-tools)
#### How to build it ####
1. Get the source
    1. mkdir fileman
    1. cd fileman
    1. gclient config https://bitbucket.org/gutmann_/fileman.git --name=src
    1. gclient sync
2. Build
    1. cd src  
    1. ninja -C out/{Debug,Release} [Module]  
3. Run server
    * ./out/Release/server server_config_example.json # with default config server start on port 9999
4. Connect to sefver
    * ./out/Release/connect localhost:9999 user1 # connect to server on port 9999 as user1