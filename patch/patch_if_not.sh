#!/bin/bash

patch -p0 --dry-run --silent $1 $2 1>/dev/null 2>&1;
if [ $? -eq 0 ]; then
  echo "patching $0 with $1";
  patch -p0 $1 $2;
fi
