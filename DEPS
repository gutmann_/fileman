# Copyright 2014 Google Inc. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# Packager dependencies.

vars = {
  "chromium_svn": "http://src.chromium.org/chrome/trunk",
  "chromium_rev": "297736",

  "googlecode_url": "http://%s.googlecode.com/svn",
  "gflags_rev": "84",
  "gmock_rev": "470",
  "gtest_rev": "680",
  "gyp_rev": "1876",
  "webrtc_rev": "5718",  # For gflags.

  "curl_url": "https://github.com/bagder/curl.git",
  "curl_rev": "curl-7_37_0",
}

deps = {
  "src/fileman/base":
    Var("chromium_svn") + "/src/base@" + Var("chromium_rev"),

  "src/fileman/build":
    Var("chromium_svn") + "/src/build@" + Var("chromium_rev"),

  # Required by base/metrics/stats_table.cc.
  "src/fileman/ipc":
    File(Var("chromium_svn") + "/src/ipc/ipc_descriptors.h@" + Var("chromium_rev")),

  # Required by base isolate dependencies, although it is compiled off.
  # Dependency chain:
  # base/base.gyp <= base/base_unittests.isolate
  #               <= base/base.isolate
  #               <= build/linux/system.isolate
  #               <= net/third_party/nss/ssl.isolate
  #               <= net/third_party/nss/ssl_base.isolate
  # We don't need to pull in the whole directory, but it doesn't seem possible
  # to just pull in the two *.isolate files (ssl.isolate and ssl_base.isolate).
  "src/fileman/net/third_party/nss":
    Var("chromium_svn") + "/src/net/third_party/nss@" + Var("chromium_rev"),

  "src/fileman/testing":
    Var("chromium_svn") + "/src/testing@" + Var("chromium_rev"),

  "src/fileman/testing/gmock":
    (Var("googlecode_url") % "googlemock") + "/trunk@" + Var("gmock_rev"),

  "src/fileman/testing/gtest":
    (Var("googlecode_url") % "googletest") + "/trunk@" + Var("gtest_rev"),

  # Required by libxml.
  "src/fileman/third_party/icu":
    Var("chromium_svn") + "/deps/third_party/icu46@" + Var("chromium_rev"),

  # Required by base/message_pump_libevent.cc.
  "src/fileman/third_party/libevent":
    Var("chromium_svn") + "/src/third_party/libevent@" + Var("chromium_rev"),

  "src/fileman/third_party/libxml":
    Var("chromium_svn") + "/src/third_party/libxml@" + Var("chromium_rev"),

  "src/fileman/third_party/modp_b64":
    Var("chromium_svn") + "/src/third_party/modp_b64@" + Var("chromium_rev"),

  # Required by build/linux/system.gyp and third_party/curl/curl.gyp.
  "src/fileman/third_party/zlib":
    Var("chromium_svn") + "/src/third_party/zlib@" + Var("chromium_rev"),

  "src/fileman/tools/clang":
    Var("chromium_svn") + "/src/tools/clang@" + Var("chromium_rev"),

  "src/fileman/tools/gyp":
    (Var("googlecode_url") % "gyp") + "/trunk@" + Var("gyp_rev"),
}

deps_os = {
  "unix": {  # Linux, actually.
    # Linux gold build to build faster.
    "src/fileman/third_party/gold":
      Var("chromium_svn") + "/deps/third_party/gold@" + Var("chromium_rev"),
  },
}

hooks = [
  {
    "pattern": "src/fileman/build/common.gypi",
    "action": ["bash", "src/patch/patch_if_not.sh",
               "src/fileman/build/common.gypi", "src/patch/common.gupi.patch"],
  },
  {
    # A change to a .gyp, .gypi, or to GYP itself should run the generator.
    "pattern": ".",
    "action": ["python", "src/gyp_fileman.py", "--depth=src/fileman"],
  },
]
