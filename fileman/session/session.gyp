# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'session',
      'type': '<(component)',
      'sources': [
        'session.cc',
        'session.h',
        'session_manager.cc',
        'session_manager.h',
      ],
      'dependencies': [
        'user',
        'server/server.gyp:libserver',
        '../command/command.gyp:command',
        '../base/base.gyp:base',
        '../message/message.gyp:message',
      ],
    },

    {
      'target_name': 'user',
      'type': '<(component)',
      'sources': [
        'environment.cc',
        'environment.h',
        'user.cc',
        'user.h',
      ],
      'dependencies': [
      ],
    },
  ],
}
