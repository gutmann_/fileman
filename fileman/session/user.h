#ifndef SESSION_USER_H_
#define SESSION_USER_H_

#include <string>

namespace fileman {
namespace session {

/// User in system
class User {
 public:
  explicit User(const std::string& name);

  const std::string& name() const;
 private:
  std::string name_;
};

} // namespace session
} // namespace fileman

#endif // SESSION_USER_H_
