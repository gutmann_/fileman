#include "fileman/session/user.h"

namespace fileman {
namespace session {

User::User(const std::string& name) : name_(name) {}
const std::string& User::name() const { return name_; }

} // namespace session
} // namespace fileman
