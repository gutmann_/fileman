#include "fileman/session/session.h"

namespace fileman {
namespace session {

Session::Session(User* user)
  : who_(user) {}
  
const User& Session::who() const { return *who_; }

Environment* Session::ENV() {
  return &env_;
}

}
}
