#ifndef SESSION_ENVIRONMENT_H_
#define SESSION_ENVIRONMENT_H_

#include "fileman/base/files/file_path.h"

namespace fileman {
namespace session {

/// Environment in which command will be executed.
class Environment {
 public:
  Environment();

  void ChangeWorkDirectory(const base::FilePath& work_directory);

  /// Current work directory.
  const base::FilePath& CWD() const;
 private:
  base::FilePath work_directory_;
};

} // namespace session
} // namespace fileman

#endif // SESSION_ENVIRONMENT_H_
