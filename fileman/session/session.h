#ifndef SESSION_SESSION_H_
#define SESSION_SESSION_H_

#include "fileman/session/user.h"
#include "fileman/session/environment.h"

namespace fileman {
namespace session {

class Session {
 public:
  explicit Session(User* user);

  const User& who() const;
  Environment* ENV();
 private:
  User* who_;
  Environment env_;
};

} // namespace session
} // namespace fileman

#endif // SESSION_SESSION_H_
