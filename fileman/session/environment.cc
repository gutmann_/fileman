#include "fileman/session/environment.h"

#include "fileman/base/logging.h"

namespace fileman {
namespace session {

Environment::Environment()
  : work_directory_("/") {}

void Environment::ChangeWorkDirectory(const base::FilePath& work_directory) {
  DCHECK(work_directory.IsAbsolute());
  work_directory_ = work_directory;
}

const base::FilePath& Environment::CWD() const {
  return work_directory_;
}

} // namespace session
} // namespace fileman
