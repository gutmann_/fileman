#ifndef SESSION_SESSION_MANAGER_H_
#define SESSION_SESSION_MANAGER_H_

#include "fileman/session/session.h"

#include <map>
#include <set>
#include <string>

namespace fileman {

namespace command {
class Executor;
}

namespace message {
class Message;
}

namespace session {

class RemoteConnection;
class Server;
class Session;

/// Session manager. Responds for users interaction with server
class SessionManager {
 public:
  /// Set message source
  /// @param server which will be listened for messages
  void ListenMessagesFrom(Server* server);

  /// Use |executor| for executing commands from clients.
  void ExecuteCommandsUsing(command::Executor* executor);
 private:
  void OnEstablishConnection(Server* server, RemoteConnection* conn);
  void OnMessage(Server* server, RemoteConnection* conn,
                 const message::Message& message);
  void OnDisconnect(Server* server, const RemoteConnection* conn);

  void OnAuthorizationFail(Server* server, RemoteConnection* conn,
                           const message::Message& message);

  Session* AuthorizedSessionFor(Server* server,
                                RemoteConnection* conn,
                                const message::Message& message);
  RemoteConnection* ConnectionForUser(const std::string& user_name);
  void OnUserAuthorized(Server* server, RemoteConnection* conn,
                        const Session* session);

  void OnCommandExecuted(const message::Message& command, int error,
                         const std::string& message);

  void OnUserDisconnected(Server* server, const RemoteConnection* conn,
                          const Session* session);

  User* RegisterUser(const User& user);
  const User* FindUserForMessage(const message::Message& message) const;

  command::Executor* executor_;
  Server* server_;

  std::set<std::string> active_users_;
  std::map<std::string, RemoteConnection*> connections_for_users_;
  std::map<const RemoteConnection*, Session> sessions_;

  // users permanent registry... storing known users who ever been logged on
  // server.
  std::map<std::string, session::User> all_users_;
};

} // namespace session
} // namespace fileman

#endif // SESSION_SESSION_MANAGER_H_
