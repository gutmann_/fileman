#include "fileman/session/session_manager.h"

#include "fileman/command/executor.h"
#include "fileman/message/message.h"
#include "fileman/session/server/remote_connection.h"
#include "fileman/session/server/server.h"
#include "fileman/session/user.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"
#include "fileman/base/strings/string_number_conversions.h"

namespace fileman {

namespace {

using message::Message;

Message NumOfUsersOnServerMessage(size_t num) {
  const char* const USERS_WORD = num != 1 ? "users" : "user";

  Message msg = Message::InfoMessage();
  msg.PutArg(base::Uint64ToString(num) + " active " + USERS_WORD +
                                         " on server");
  return msg;
}

} // anonimous namespace

namespace session {

void SessionManager::ListenMessagesFrom(Server* server) {
  DCHECK(server);

  server_ = server;
  server->OnEstablishConnection(
      base::Bind(&SessionManager::OnEstablishConnection,
                 base::Unretained(this)));
  server->OnMessage(base::Bind(&SessionManager::OnMessage,
                               base::Unretained(this)));
  server->OnDisconnect(base::Bind(&SessionManager::OnDisconnect,
                                  base::Unretained(this)));
}

void SessionManager::ExecuteCommandsUsing(command::Executor* executor) {
  DCHECK(executor);

  executor_ = executor;
  executor_->OnCommandExecuted(base::Bind(&SessionManager::OnCommandExecuted,
                                          base::Unretained(this)));
}

void SessionManager::OnEstablishConnection(Server* server,
                                           RemoteConnection* conn) {}

void SessionManager::OnMessage(Server* server, RemoteConnection* conn,
                               const message::Message& message) {
  Session* session = AuthorizedSessionFor(server, conn, message);
  if (!session) {
    OnAuthorizationFail(server, conn, message);
  } else {
    DCHECK(executor_);
    if (message::CouldBeExecuted(message.opcode())) {
      const User* user = FindUserForMessage(message);
      if (user) {
        executor_->Execute(message, *user, session->ENV());
      } else {
        LOG(WARNING) << "Got message from unregistered user \""
                     << message.Sender() << '\"';
        conn->Send(message::Message::MakeResponseTo(message, 1,
                                                    "you are unregistered user"));
      }
    } else if (message.opcode() != message::AUTH) {
      conn->Send(message::Message::MakeResponseTo(message, 1,
                                                  "unknown or bad message"));
    }
  }
}

void SessionManager::OnDisconnect(Server* server,
                                  const RemoteConnection* conn) {
  std::map<const RemoteConnection*, Session>::iterator i =
    sessions_.find(conn);
  if (i != sessions_.end()) {
    LOG(INFO) << "User \"" << i->second.who().name() 
              << "\" has been disconnected";
    OnUserDisconnected(server, conn, &i->second);

    size_t num_of_erased = connections_for_users_.erase(i->second.who().name());
    DCHECK_GT(num_of_erased, 0u);

    num_of_erased = active_users_.erase(i->second.who().name());
    DCHECK_GT(num_of_erased, 0u);
    sessions_.erase(i);
  } else {
    LOG(INFO) << "Closing unauthorized connection";
  }
}

void SessionManager::OnAuthorizationFail(Server* server, RemoteConnection* conn,
                                         const message::Message& message) {
  message::Message auth_failed_message =
      message::Message::MakeResponseTo(message, 1, "authorization failed");
  conn->Send(auth_failed_message);
  // conn->Disconnect();
}

Session* SessionManager::AuthorizedSessionFor(Server* server,
                                              RemoteConnection* conn,
                                              const message::Message& message) {
  const std::string user_name = message.Sender();
  std::map<const RemoteConnection*, Session>::iterator i =
    sessions_.find(conn);

  if (i != sessions_.end()) { // there are an active session
    Session* known_session = &i->second;
    if (known_session->who().name() == user_name) // from the known user
      return known_session;
    else
      return NULL;
  } else if (message.opcode() == message::AUTH) {
    // somebody tries to auth using already active username
    if (active_users_.find(user_name) != active_users_.end())
      return NULL;

    const bool user_was_inserted =
      active_users_.insert(user_name).second;

    DCHECK_EQ(user_was_inserted, true);

    LOG(INFO) << "New user \"" << user_name << "\" authorized on server";

    DCHECK(connections_for_users_.find(user_name) ==
           connections_for_users_.end());

    User* user = RegisterUser(User(user_name));
    DCHECK(user);

    connections_for_users_.insert(std::make_pair(user_name, conn));
    std::map<const RemoteConnection*, Session>::iterator new_session =
      sessions_.insert(std::make_pair(conn, Session(user)))
      .first;

    OnUserAuthorized(server, conn, &new_session->second);
    return &new_session->second;
  }
  return NULL;
}

RemoteConnection* SessionManager::ConnectionForUser(const std::string& user_name) {
  std::map<std::string, RemoteConnection*>::iterator i =
    connections_for_users_.find(user_name);
  if (i != connections_for_users_.end())
    return i->second;
  return NULL;
}

void SessionManager::OnUserAuthorized(Server* server,
                                      RemoteConnection* conn,
                                      const Session* session) {
  conn->Send(NumOfUsersOnServerMessage(active_users_.size()));
}

void SessionManager::OnCommandExecuted(const message::Message& command,
                                       int error,
                                       const std::string& message) {
  RemoteConnection* conn = ConnectionForUser(command.Sender());
  if (conn) {
    LOG(INFO) << "Command \"" << command.AsString() << "\" was executed";
    conn->Send(message::Message::MakeResponseTo(command, error, message));
  } else {
    LOG(WARNING) << "Can't return execution result because user "
                    "already disconnected";
  }

  if (!error) {
    const std::string message_to_others =
      command.Sender() + " performs command: " + command.AsString();
    server_->Broadcast(message::Message::InfoMessage(message_to_others),
                       conn);
  }
}

void SessionManager::OnUserDisconnected(Server* server,
                                        const RemoteConnection* conn,
                                        const Session* session) {
  const std::string message_to_others =
    session->who().name() + " performs command: Quit";
  server->Broadcast(message::Message::InfoMessage(message_to_others),
                    conn);
}

User* SessionManager::RegisterUser(const session::User& user) {
  return
    &all_users_.insert(std::make_pair(user.name(), user)).first->second;
}

const User*
    SessionManager::FindUserForMessage(const message::Message& message) const {
  std::map<std::string, session::User>::const_iterator i =
    all_users_.find(message.Sender());
  if (i != all_users_.end()) return &i->second;
  return NULL;
}

} // namespace session
} // namespace fileman
