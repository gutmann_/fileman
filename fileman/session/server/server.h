#ifndef SESSION_SERVER_SERVER_H_
#define SESSION_SERVER_SERVER_H_

#include "fileman/base/callback.h"

#include "fileman/third_party/libevent/event.h"

#include <arpa/inet.h>

#include <set>
#include <string>
#include <vector>

namespace fileman {

namespace message {
class Message;
} // namespace message

namespace session {

class RemoteConnection;

class Server {
 public:
  typedef base::Callback<void(Server* server, RemoteConnection* conn)>
      OnEstablishConnectionCB;
  typedef base::Callback<void(Server* server, RemoteConnection* conn,
                              const message::Message& message)>
      OnMessageCB;
  typedef base::Callback<void(Server* server, const RemoteConnection* conn)>
      OnDisconnectCB;

  Server(const std::string& iface, int port);
  ~Server();

  bool Init();
  bool Bind();
  bool Listen();

  bool Initialized() const;

  void Broadcast(const message::Message& message,
                 const RemoteConnection* except = NULL);

  void OnEstablishConnection(const OnEstablishConnectionCB& cb);
  void OnMessage(const OnMessageCB& cb);
  void OnDisconnect(const OnDisconnectCB& cb);
 private:
  bool InitSocket();
  bool InitSin();

  static void OnAccept(int fd, short ev, void *arg);
  void OnNewConnection();
  void OnDisconnectFrom(const RemoteConnection* conn);
  void OnMessageFrom(RemoteConnection* conn, const message::Message& msg);

  const std::string iface_;
  const int port_;

  int sock_fd_;
  sockaddr_in sin_;

  event ev_;

  std::set<RemoteConnection*> connections_;

  OnEstablishConnectionCB on_establish_cb_;
  OnMessageCB on_message_cb_;
  OnDisconnectCB on_disconnect_cb_;

  bool __initialized;
};

} // namespace session
} // namespace fileman

#endif // SESSION_SERVER_SERVER_H_
