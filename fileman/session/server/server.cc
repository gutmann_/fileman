#include "fileman/session/server/server.h"

#include "fileman/session/server/remote_connection.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"
#include "fileman/base/memory/scoped_ptr.h"
#include "fileman/base/stl_util.h"

#include <unistd.h>

namespace fileman {
namespace session {

namespace {

enum { kBacklogSize = 1000 };

void DefaultOnEstablishedConnectionCallback(Server* server,
                                            RemoteConnection* conn) {
  DVLOG(3) << "You doesn't set `OnEstablishConnection` callback";
}

void DefaultOnMessageCallback(Server* server, RemoteConnection* conn,
                                              const message::Message& message) {
  DVLOG(3) << "You doesn't set `OnMessage` callback";
}

void DefaultOnDisconnectCallback(Server* server, const RemoteConnection* conn) {
  DVLOG(3) << "You doesn't set `OnDisconnect` callback";
}

} // anonimous namespace

Server::Server(const std::string& iface, int port)
  : iface_(iface),
    port_(port),
    sock_fd_(0),
    sin_(),
    ev_(),
    connections_(),
    on_establish_cb_(base::Bind(&DefaultOnEstablishedConnectionCallback)),
    on_message_cb_(base::Bind(&DefaultOnMessageCallback)),
    on_disconnect_cb_(base::Bind(&DefaultOnDisconnectCallback)),
    __initialized(false) {
  bzero(&sin_, sizeof(sin_));
  bzero(&ev_, sizeof(ev_));
}

Server::~Server() {
  STLDeleteElements(&connections_);

  event_del(&ev_);
  if (sock_fd_ > 0) close(sock_fd_);
}

bool Server::Init() {
  DCHECK(!Initialized());
  __initialized = InitSocket() && InitSin();
  return __initialized;
}

bool Server::Bind() {
  DCHECK(Initialized());

  LOG(INFO) << "Binding server to " << iface_ << ':' << port_;
  int err = bind(sock_fd_, (struct sockaddr*)&sin_, sizeof(sin_));
  if (err) {
    LOG(ERROR) << "Error #" << err << " while trying to bind server to port "
               << port_;
    return false;
  } else {
    LOG(INFO) << "Socket successfully binded to port " << port_;
  }
  return true;
}

bool Server::Listen() {
  DCHECK(Initialized());

  int listen_err = listen(sock_fd_, kBacklogSize);
  if (listen_err < 0) {
    LOG(ERROR) << "Error while trying to listen socket @ port " << port_;
    return false;
  } else {
    LOG(INFO) << "Start listening socket @ port " << port_;
  }

  event_set(&ev_, sock_fd_, EV_READ | EV_PERSIST, OnAccept, this);
  if (event_add(&ev_, 0) != 0) {
    LOG(ERROR) << "Can't schedule accept event";
    return false;
  }
  return true;
}

bool Server::Initialized() const { return __initialized; }

void Server::Broadcast(const message::Message& message,
                       const RemoteConnection* except) {
  for(std::set<RemoteConnection*>::iterator i(connections_.begin());
      i != connections_.end(); ++i) {
    if (*i != except)
      (*i)->Send(message);
  }
}

void Server::OnEstablishConnection(const OnEstablishConnectionCB& cb) {
  on_establish_cb_ = cb;
}

void Server::OnMessage(const OnMessageCB& cb) {
  on_message_cb_ = cb;
}

void Server::OnDisconnect(const OnDisconnectCB& cb) {
  on_disconnect_cb_ = cb;
}

bool Server::InitSocket() {
  DCHECK(!Initialized());

  sock_fd_ = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd_ < 0) {
    LOG(ERROR) << "Can't create socket descriptor";
    return false;
  }
  return true;
}

bool Server::InitSin() {
  DCHECK(!Initialized());

  memset(&sin_, 0, sizeof(sin_));
  sin_.sin_family = AF_INET;
  sin_.sin_addr.s_addr = inet_addr(iface_.c_str());
  sin_.sin_port = htons(port_);

  return true;
}

// static
void Server::OnAccept(int fd, short ev, void *arg) {
  DCHECK(arg);

  Server* this_ = static_cast<Server*>(arg);
  this_->OnNewConnection();
}

void Server::OnNewConnection() {
  sockaddr_in addr;
  socklen_t len = sizeof(addr);
  int fd = accept(sock_fd_,
                  reinterpret_cast<sockaddr*>(&addr),
                  &len);
  if (fd < 0) {
    LOG(ERROR) << "Error while accepting new connection";
    // Stop();
  }

  scoped_ptr<RemoteConnection> new_connection(
      new RemoteConnection(fd, addr, this));
  if (new_connection->Initialize()) {
    new_connection->OnDisconnect(base::Bind(&Server::OnDisconnectFrom,
                                            base::Unretained(this)));
    new_connection->OnMessage(base::Bind(&Server::OnMessageFrom,
                                         base::Unretained(this)));

    RemoteConnection* new_connection_ptr = new_connection.get();
    connections_.insert(new_connection.release());
    on_establish_cb_.Run(this, new_connection_ptr);
  } else {
    LOG(ERROR) << "Can't initialize connection";
  }
}

void Server::OnDisconnectFrom(const RemoteConnection* conn) {
  DCHECK(conn);
  RemoteConnection* connection = const_cast<RemoteConnection*>(conn);

  std::set<RemoteConnection*>::iterator conn_in_pool =
    connections_.find(connection);
  DCHECK(conn_in_pool != connections_.end());

  scoped_ptr<RemoteConnection> to_remove(*conn_in_pool);
  connections_.erase(conn_in_pool);
  on_disconnect_cb_.Run(this, to_remove.get());
  to_remove.reset();
}

void Server::OnMessageFrom(RemoteConnection* conn, const message::Message& msg) {
  DCHECK(conn);
  on_message_cb_.Run(this, conn, msg);
}

} // namespace session
} // namespace fileman
