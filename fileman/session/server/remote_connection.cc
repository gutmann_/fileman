#include "fileman/session/server/remote_connection.h"

#include "fileman/session/server/server.h"

#include "fileman/message/buffer_reader.h"
#include "fileman/message/buffer_writer.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"

#include <stdlib.h>
#include <unistd.h>

namespace {

enum { kReadChunkSize = 4096 };

} // anonimous namespace

namespace fileman {
namespace session {

namespace {

void DefaultOnInvalidMessageCallback(RemoteConnection* emitter,
                                     const std::vector<uint8_t>& data) {
  LOG(ERROR) << "Got invalid message (" << data.size() << " bytes)";
}

} // anonimous namespace

RemoteConnection::RemoteConnection(int fd, const sockaddr_in& addr,
                                   Server* server)
  : fd_(fd),
    addr_(addr),
    sock_bufevent_(NULL),
    state_(kNotInitialized),
    stream_parser_(),
    on_message_cb_(),
    on_invalid_message_cb_(base::Bind(&DefaultOnInvalidMessageCallback)),
    on_close_cb_() {
  stream_parser_.OnMessage(base::Bind(&RemoteConnection::ProcessMessage,
                                      base::Unretained(this)));
  stream_parser_.OnStreamParseError(
      base::Bind(&RemoteConnection::ProcessStreamParseError,
                 base::Unretained(this)));
}

RemoteConnection::~RemoteConnection() {
  if (state() != kDisconnected)
    Disconnect();
}

bool RemoteConnection::Initialize() {
  DCHECK(state() == kNotInitialized);
  DCHECK(sock_bufevent_ == NULL);

  sock_bufevent_ =
    bufferevent_new(fd_,
                    &OnReadCB,
                    NULL,
                    &OnErrorCB,
                    this);
  if (sock_bufevent_ == NULL) {
    LOG(ERROR) << "Can't create bufferevent";
    return false;
  }

  if (bufferevent_enable(sock_bufevent_, EV_READ) != 0) {
    LOG(ERROR) << "Can't enable eventuffer";
    return false;
  }

  state_ = kConnected;
  return true;
}

void RemoteConnection::Send(const message::Message& message) {
  DCHECK(state() == kConnected);

  message::BufferWriter writer;
  message.WriteTo(&writer);
  size_t write =
    bufferevent_write(sock_bufevent_, writer.Buffer(), writer.Size());

  if (write < 0) {
    LOG(ERROR) << "Write error";
    Disconnect();
  }
}

bool RemoteConnection::Disconnect() {
  DVLOG(3) << "Disconnect";

  if (state() != kConnected) {
    LOG(WARNING) << "You trying to disconnect not connected socket";
  }

  FreeEventHandling();
  int sock_close_status = close(fd_);
  if (state() != kDisconnected) {
    state_ = kDisconnected;
    on_close_cb_.Run(this);
  }

  return sock_close_status == 0;
}

RemoteConnection::State RemoteConnection::state() const { return state_; }

std::string RemoteConnection::Address() const {
  char buf[128];
  const char* addr_text = inet_ntop(AF_INET, &addr_, buf, sizeof(addr_));
  if (!addr_text) {
    LOG(ERROR) << "Can't convert add to string representation";
    return std::string();
  }
  return std::string(addr_text);
}

int RemoteConnection::Port() const { return ntohs(addr_.sin_port); }

void RemoteConnection::OnMessage(const OnMessageCB& cb) {
  on_message_cb_ = cb;
}

void RemoteConnection::OnInvalidMessage(const OnOnvalidMessageCB& cb) {
  on_invalid_message_cb_ = cb;
}

void RemoteConnection::OnDisconnect(const OnCloseCB& cb) {
  on_close_cb_ = cb;
}

void RemoteConnection::FreeEventHandling() {
  if (sock_bufevent_) {
    if (bufferevent_disable(sock_bufevent_, EV_READ) != 0) {
      LOG(ERROR) << "Can't disable eventbuffer";
    }
    bufferevent_free(sock_bufevent_);
    sock_bufevent_ = NULL;
  }
}

// static
void RemoteConnection::OnReadCB(bufferevent* bevent, void* arg) {
  DCHECK(arg);
  RemoteConnection* this_ = reinterpret_cast<RemoteConnection*>(arg);
  this_->ProcessRead(bevent);
}

// static
void RemoteConnection::OnErrorCB(bufferevent* bevent, short what, void* arg) {
  DCHECK(arg);
  RemoteConnection* this_ = reinterpret_cast<RemoteConnection*>(arg);
  this_->ProcessError(bevent, what);
}

void RemoteConnection::ProcessRead(bufferevent* bevent) {
  DCHECK(state() == kConnected);
  DCHECK_EQ(bevent, sock_bufevent_);
  DVLOG(4) << "ProcessRead";

  while (true) {
    uint8_t data[kReadChunkSize];
    const size_t read = bufferevent_read(bevent, data, sizeof(data));
    if (read > 0)
      stream_parser_.Push(data, read);
    else
      break;
  }
}

void RemoteConnection::ProcessError(bufferevent* bevent, short what) {;
  /*
  const bool remove_host_closed_connection =
    what & EVBUFFER_EOF;
  if (remove_host_closed_connection)*/
  Disconnect();
}

void RemoteConnection::ProcessStreamParseError(
      message::Message::Status parse_status, const std::vector<uint8_t>& msg) {
  Disconnect();
}

void RemoteConnection::ProcessMessage(const message::Message& message) {
  on_message_cb_.Run(this, message);
}

} // namespace session
} // namespace fileman
