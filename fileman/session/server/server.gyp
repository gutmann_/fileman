# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'libserver',
      'type': '<(component)',
      'sources': [
        'remote_connection.cc',
        'remote_connection.h',
        'server.cc',
        'server.h',
      ],
      'dependencies': [
        '../../message/message.gyp:message',
        '../../base/base.gyp:base',
        '../../third_party/libevent/libevent.gyp:libevent',
      ],
    },
  ],
}
