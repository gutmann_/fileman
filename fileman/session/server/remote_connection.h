#ifndef SESSION_SERVER_REMOVE_CONNTION_H_
#define SESSION_SERVER_REMOVE_CONNTION_H_

#include "fileman/message/message.h"
#include "fileman/message/stream_parser.h"

#include "fileman/base/callback.h"

#include "fileman/third_party/libevent/event.h"
#include <arpa/inet.h>

#include <stdint.h>
#include <string>
#include <vector>

namespace fileman {
namespace session {

class Server;

class RemoteConnection {
 public:
  enum State {
    kNotInitialized = 1,
    kInitialized    = 2,
    kConnected      = 3,
    kDisconnected   = 4,
    kDestroyed      = 5,
  };

  typedef base::Callback<void(RemoteConnection* emitter,
                              const message::Message& msg)>
      OnMessageCB;
  typedef base::Callback<void(RemoteConnection* emitter,
                              const std::vector<uint8_t>& data)>
      OnOnvalidMessageCB;
  typedef base::Callback<void(const RemoteConnection* conn)>
      OnCloseCB;

      

  RemoteConnection(int fd, const sockaddr_in& addr, Server* server);
  ~RemoteConnection();

  bool Initialize();

  void Send(const message::Message& message);

  bool Disconnect();

  State state() const;
  std::string Address() const;
  int Port() const;

  void OnMessage(const OnMessageCB& cb);
  void OnInvalidMessage(const OnOnvalidMessageCB& cb);
  void OnDisconnect(const OnCloseCB& cb);
 private:
  void FreeEventHandling();

  static void OnReadCB(bufferevent* bevent, void* arg);
  static void OnErrorCB(bufferevent* bevent, short what, void* arg);

  void ProcessRead(bufferevent* bevent);
  
  void ProcessError(bufferevent* bevent, short what);
  void ProcessStreamParseError(message::Message::Status parse_status,
                               const std::vector<uint8_t>& msg);


  void ProcessMessage(const message::Message& message);
  
  const int fd_;
  sockaddr_in addr_;

  bufferevent* sock_bufevent_;

  State state_;

  message::StreamParser stream_parser_;

  OnMessageCB on_message_cb_;
  OnOnvalidMessageCB on_invalid_message_cb_;
  OnCloseCB on_close_cb_;
};

} // namespace session
} // namespace fileman

#endif // SESSION_SERVER_REMOVE_CONNTION_H_
