#include "fileman/vfs/file.h"

#include "fileman/base/logging.h"
#include "fileman/vfs/visitor.h"

namespace fileman {
namespace vfs {

File::File() : Object(kFile) {}

File::~File() {
  DVLOG(3) << "Destroying file \"" << name() << "\".";
}

scoped_ptr<Object> File::Clone() const {
  return scoped_ptr<Object>(new File(*this));
}

void File::Visit(Visitor* visitor) {
  visitor->OnEnter(this);
  visitor->OnLeave(this);
}

void File::Visit(ConstVisitor* visitor) const {
  visitor->OnEnter(this);
  visitor->OnLeave(this);
}

File::File(const File& src) : Object(src) {}

} // namespace vfs
} // namespace fileman
