#ifndef VFS_UTIL_H_
#define VFS_UTIL_H_

#include "fileman/vfs/directory.h"

#include "fileman/base/memory/scoped_ptr.h"

namespace fileman {
namespace vfs {

class Filesystem;

/// Move |what| object into |into|. If |what and |into| are files
/// into will be overwritten with |what| (but if |into| is not locked).
///
/// If objects can't be fully merged then merge conflicts place into
/// |conflicts|. This following cases are merge conflicts:
///  1. Type of |what| and object with |what->name()| in |into| children
///     has different types (file and directory or vice versa)
///  2. |what| are locked.
///  3. Child of |into| with name |what->name()| are locked
/// This rules above applies to any child of |what| and |into|
/// @param what -- what will be moved
/// @param into -- place for |what|
/// @param conflicts -- place for merge conflicts
void Move(scoped_ptr<Object> what, Object* into,
          scoped_ptr<Object>* conflicts,
          const Filesystem& fs);


struct UnaryPredicate {
  virtual bool operator ()(const Object* o) = 0;
};

/// Perform deletion of object |o| if it conforms to |predicate|
/// This method recursively walk through objects tree.
/// @param o -- object to delete
/// @param predicate -- predicate that determines deletion rule (delete or not)
/// @return not deleted object or NULL if all objects has been deleted.
scoped_ptr<Object> DeleteIf(scoped_ptr<Object> o, UnaryPredicate* predicate);

void Print(const Directory& directory);

} // namespace vfs
} // namespace fileman

#endif // VFS_UTIL_H_
