#include "fileman/vfs/vfs_util.h"

#include "fileman/vfs/directory.h"
#include "fileman/vfs/file.h"
#include "fileman/vfs/filesystem.h"
#include "fileman/vfs/visitor.h"

#include "fileman/base/logging.h"

#include <iostream>

namespace fileman {
namespace vfs {

namespace {

class PrintTreeVisitor : public ConstVisitor {
 public:
  PrintTreeVisitor();

  virtual void OnEnter(const File* file);
  virtual void OnLeave(const File* file);

  virtual void OnEnter(const Directory* dir);
  virtual void OnLeave(const Directory* dir);
 private:
  void Print(const Object* o);

  int level_;
};

PrintTreeVisitor::PrintTreeVisitor() : level_(0) {}

void PrintTreeVisitor::OnEnter(const File* file) { Print(file); ++level_; }
void PrintTreeVisitor::OnLeave(const File* file) { --level_; }

void PrintTreeVisitor::OnEnter(const Directory* dir) { Print(dir); ++level_; }
void PrintTreeVisitor::OnLeave(const Directory* dir) { --level_; }

void PrintTreeVisitor::Print(const Object* o) {
  DCHECK(o);

  for (int l = 0; l < level_; ++l)
    std::cout << (l < level_ - 1 ? "| " : "|_");
  std::cout << (o->name().empty() ? "/" : o->name()) << std::endl;
}



struct SortByName {
  bool operator ()(const Object* lhs, const Object* rhs) const {
    DCHECK(lhs);
    DCHECK(rhs);

    return lhs->name() < rhs->name();
  }
};

template<typename T>
struct remove_pointer {
  typedef T type;
};

template<typename T>
struct remove_pointer<T*> {
  typedef typename remove_pointer<T>::type type;
};

template <class ScopedVectorIterator>
scoped_ptr
<
  typename remove_pointer
  <
    typename std::iterator_traits<ScopedVectorIterator>::value_type
  >::type
>

TakeOwnershipFrom(ScopedVectorIterator i) {
  typedef typename remove_pointer
  <
    typename std::iterator_traits<ScopedVectorIterator>::value_type
  >::type T;

  T* p = *i;
  *i = NULL;
  return scoped_ptr<T>(p);
}


#define REGISTER_CONFLICT(i)    \
  do {                          \
    if (!(*conflicts)) {        \
      *conflicts = rhs.Pass();  \
    }                           \
    (*conflicts)->LinkChild(i); \
  } while (0)

#define DCHECK_FILLED_WITH_NULLS(v) \
  DCHECK_EQ                         \
  (                                 \
   static_cast<size_t>              \
   (                                \
    std::count                      \
    (                               \
     v.begin(), v.end(),            \
     static_cast<Object*>(NULL)     \
    )                               \
   ),                               \
   v.size()                         \
  )

scoped_ptr<Directory> MergeDirectories(scoped_ptr<Directory> lhs,
                                       scoped_ptr<Directory> rhs,
                                       scoped_ptr<Directory>* conflicts,
                                       const Filesystem& fs) {
  DCHECK(lhs);
  DCHECK(rhs);
  DCHECK(conflicts);
  DCHECK(lhs->name() == lhs->name());

  ScopedVector<Object> lhs_children = lhs->TakeChildren();
  ScopedVector<Object> rhs_children = rhs->TakeChildren();

  std::sort(lhs_children.begin(), lhs_children.end(), SortByName());
  std::sort(rhs_children.begin(), rhs_children.end(), SortByName());

  scoped_ptr<Directory> root(lhs.Pass());

  ScopedVector<Object>::iterator i(lhs_children.begin()),
                                 j(rhs_children.begin());

  // same approach as in MERGE part of MERGE SORT
  while (i != lhs_children.end() && j != rhs_children.end()) {
    scoped_ptr<Object> child;
    if ((*i)->name() < (*j)->name()) {
      if (!fs.lock_set().WhoLockedObject(*i).empty()) {
        REGISTER_CONFLICT(TakeOwnershipFrom(i++));
        continue;
      } else {
        child = TakeOwnershipFrom(i++);
      }
    } else if ((*i)->name() > (*j)->name()) {
      child = TakeOwnershipFrom(j++);
    } else { // name collision
      if ((*i)->type() == Object::kFile && (*j)->type() == Object::kFile) {
        if (!fs.lock_set().WhoLockedObject(*j).empty()) {
          // we can't overwrite locked files
          child = TakeOwnershipFrom(j);
          REGISTER_CONFLICT(TakeOwnershipFrom(i));
        } else { // ok overwrite it
          child = TakeOwnershipFrom(i);
          TakeOwnershipFrom(j); // releasing it
        }
      } else if ((*i)->type() == Object::kDirectory &&
                 (*j)->type() == Object::kDirectory) {
        scoped_ptr<Object> i_dir = TakeOwnershipFrom(i),
                           j_dir = TakeOwnershipFrom(j);

        scoped_ptr<Directory> child_conflicts;
        child = MergeDirectories(
            scoped_ptr<Directory>(static_cast<Directory*>(i_dir.release())),
            scoped_ptr<Directory>(static_cast<Directory*>(j_dir.release())),
            &child_conflicts, fs);

        if (child_conflicts) {
          REGISTER_CONFLICT(scoped_ptr<Object>(child_conflicts.Pass()));
        }
      } else {
        child = TakeOwnershipFrom(j);
        REGISTER_CONFLICT(TakeOwnershipFrom(i));
      }

      ++i, ++j;
    }

    DCHECK(child);
    root->LinkChild(child.Pass());
  }

  // copying the rest...
  while (i != lhs_children.end()) {
    if (!fs.lock_set().WhoLockedObject(*i).empty()) {
      REGISTER_CONFLICT(TakeOwnershipFrom(i));
    } else {
      root->LinkChild(TakeOwnershipFrom(i));
    }
    ++i;
  }
  while (j != rhs_children.end())
    root->LinkChild(TakeOwnershipFrom(j++));

  // and check (in DEBUG_BUILD) that we don't forget something.
  DCHECK_FILLED_WITH_NULLS(lhs_children);
  DCHECK_FILLED_WITH_NULLS(rhs_children);

  lhs_children.weak_clear();
  rhs_children.weak_clear();

  return root.Pass();
}

void MoveFile(scoped_ptr<Object> what, Object* into,
              scoped_ptr<Object>* conflicts,
              const Filesystem& fs) {
  DCHECK(what->type() == Object::kFile);
  DCHECK(into->type() == Object::kFile);

  if (!fs.lock_set().WhoLockedObject(into).empty()) {
    *conflicts = what.Pass();
  } else {
    Directory* p = into->parent();
    p->UnlinkChild(what->name());
    p->LinkChild(what.Pass());
  }
}

void MoveDirectory(scoped_ptr<Object> what, Object* _into,
                   scoped_ptr<Object>* conflicts,
                   const Filesystem& fs) {
  DCHECK(what->type() == Object::kDirectory);
  DCHECK(_into->type() == Object::kDirectory);

  Directory* const into = static_cast<Directory*>(_into);

  scoped_ptr<Object> dst(into->UnlinkChild(what->name()));

  if (!dst) {
    into->LinkChild(what.Pass());
  } else if (dst->type() != Object::kDirectory) {
    *conflicts = what.Pass();
    into->LinkChild(dst.Pass());
  } else {
    scoped_ptr<Directory> what_dir(static_cast<Directory*>(what.release())),
                          to_merge_with(static_cast<Directory*>(dst.release()));

    scoped_ptr<Directory> conflicts_dir;
    scoped_ptr<Directory> merged = MergeDirectories(what_dir.Pass(),
                                                    to_merge_with.Pass(),
                                                    &conflicts_dir,
                                                    fs);
    *conflicts = scoped_ptr<Object>(conflicts_dir.Pass());
    into->LinkChild(scoped_ptr<Object>(merged.Pass()));
  }
}

} // anonimous namespace

void Move(scoped_ptr<Object> what, Object* into,
          scoped_ptr<Object>* conflicts,
          const Filesystem& fs) {
  if (what->type() == Object::kFile && into->type() == Object::kFile) {
    MoveFile(what.Pass(), into, conflicts, fs);
  } else if (what->type() == Object::kDirectory &&
             into->type() == Object::kDirectory) {
    MoveDirectory(what.Pass(), into, conflicts, fs);
  } else if (what->type() == Object::kFile &&
             into->type() == Object::kDirectory) {
    Directory* into_dir = static_cast<Directory*>(into);
    Object* into_obj = into_dir->ChildWithName(what->name());
    if (!into_obj) {
      File* created = NULL;
      Directory::CreateFile(into_dir, what->name(), &created);
      into_obj = created;
    }

    Move(what.Pass(), into_obj, conflicts, fs);
  } else {
    *conflicts = what.Pass();
  }
}

scoped_ptr<Object> DeleteIf(scoped_ptr<Object> o, UnaryPredicate* predicate) {
  if (o->type() == Object::kFile) {
    if (!(*predicate)(o.get()))
      return o.Pass();
  } else if (o->type() == Object::kDirectory) {
    scoped_ptr<Directory> dir(static_cast<Directory*>(o.release()));
    ScopedVector<Object> children = dir->TakeChildren();
    for (ScopedVector<Object>::iterator i(children.begin());
         i != children.end(); ++i) {
      scoped_ptr<Object> c = DeleteIf(TakeOwnershipFrom(i), predicate);
      if (c)
        dir->LinkChild(c.Pass());
    }
    children.weak_clear();

    if (!(*predicate)(dir.get()))
      return scoped_ptr<Object>(dir.Pass());
  }

  return scoped_ptr<Object>();
}

void Print(const Directory& directory) {
  PrintTreeVisitor printer;
  directory.Visit(&printer);
}

} // namespace vfs
} // namespace fileman
