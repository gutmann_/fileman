#include "fileman/vfs/directory.h"

#include "fileman/base/logging.h"
#include "fileman/base/stl_util.h"
#include "fileman/base/strings/string_util.h"
#include "fileman/vfs/file.h"
#include "fileman/vfs/visitor.h"

#define DO_OR_RETURN(func)           \
  do {                               \
    Status status = func;            \
    if (status != OK) return status; \
  } while (0)

namespace fileman {
namespace vfs {

namespace {

std::string RootDirName() {
  std::vector<std::string> n;
  n.reserve(2);
  n.push_back(""); n.push_back("");
  return JoinString(n, base::FilePath::kSeparators[0]);
}

const std::string ROOT_DIR_NAME = RootDirName();

} // anonimous namespace

// static
const base::FilePath Directory::ROOT_PATH = base::FilePath(ROOT_DIR_NAME);

Directory::Directory() : Object(kDirectory) {
  const Status rename_status = RenameTo(ROOT_DIR_NAME);
  DCHECK_EQ(rename_status, OK);
}

Directory::~Directory() {
  DVLOG(3) << "Destroying directory \"" << name() << "\".";
  STLDeleteValues(&children_);
  DVLOG(3) << "Destroying directory \"" << name() << "\" done.";
}

// static
Status Directory::CreateFile(Directory* where, const std::string& name,
                             File** result) {
  return CreateObject(where, name, result);
}

// static
Status Directory::CreateDirectory(Directory* where, const std::string& name,
                                  Directory** result) {
  return CreateObject(where, name, result);
}

// static
Status Directory::CreateObject(Directory* where, const std::string& name,
                               Object::Type type, Object** result) {
  Object* r = NULL;
  Status s = UNKNOWN_ERROR;

  switch (type) {
    case kFile: {
      File* f;
      s = CreateFile(where, name, &f);
      r = f;
      break;
    }
    case kDirectory: {
      Directory* d;
      s = CreateDirectory(where, name, &d);
      r = d;
      break;
    }
    case kBadObject: {
      LOG(ERROR) << "Can't create object of `bad` type";
      break;
    }
  }

  if (result)
    *result = r;
  return s;
}

Object* Directory::ChildWithName(const std::string& name) {
  std::map<std::string, Object*>::iterator it = children_.find(name);
  if (it != children_.end()) return it->second;
  return NULL;
}

const Object* Directory::ChildWithName(const std::string& name) const {
  std::map<std::string, Object*>::const_iterator it = children_.find(name);
  if (it != children_.end()) return it->second;
  return NULL;
}

scoped_ptr<Object> Directory::Clone() const {
  scoped_ptr<Directory> clone_of_this(new Directory(*this));
  DCHECK(clone_of_this->parent() != parent());

  for (std::map<std::string, Object*>::const_iterator i(children_.begin());
       i != children_.end(); ++i)
    clone_of_this->LinkChild(i->second->Clone().Pass());

  return scoped_ptr<Object>(clone_of_this.Pass());
}

void Directory::Visit(Visitor* visitor) {
  if (!visitor->ShouldEnter(this)) return;

  visitor->OnEnter(this);
  if (visitor->ShouldVisitChildren(this)) {
    for (std::map<std::string, Object*>::iterator i(children_.begin());
         i != children_.end(); ++i)
      i->second->Visit(visitor);
  }
  visitor->OnLeave(this);
}

void Directory::Visit(ConstVisitor* visitor) const {
  if (!visitor->ShouldEnter(this)) return;

  visitor->OnEnter(this);
  if (visitor->ShouldVisitChildren(this)) {
    for (std::map<std::string, Object*>::const_iterator i(children_.begin());
         i != children_.end(); ++i)
      i->second->Visit(visitor);
  }
  visitor->OnLeave(this);
}

bool Directory::Empty() const { return children_.empty(); }

std::vector<Object*> Directory::children() {
  std::vector<Object*> ret;
  ret.reserve(children_.size());

  for (std::map<std::string, Object*>::iterator i(children_.begin());
       i != children_.end(); ++i)
    ret.push_back(i->second);

  return ret;
}

std::vector<const Object*> Directory::children() const {
  std::vector<const Object*> ret;
  ret.reserve(children_.size());

  for (std::map<std::string, Object*>::const_iterator i(children_.begin());
       i != children_.end(); ++i)
    ret.push_back(i->second);

  return ret;
}

Directory::Directory(const Directory& src) 
  : Object(src),
    children_() {}

Status Directory::EnsureChildExists(const std::string& name) const {
  if (ChildWithName(name) == NULL) {
    return OBJECT_NOT_EXISTS;
  }
  return OK;
}

Status Directory::EnsureChildNotExists(const std::string& name) const {
  if (ChildWithName(name) != NULL) {
    return OBJECT_EXISTS;
  }
  return OK;
}

Status Directory::LinkChild(scoped_ptr<Object> child) {
  DCHECK(child->parent() == NULL);
  if (base::FilePath(child->name()) == Directory::ROOT_PATH)
    return BAD_NAME;

  DO_OR_RETURN(EnsureChildNotExists(child->name()));

  child->LinkWithParent(this);

  const std::string& name = child->name();
  children_.insert(std::make_pair(name, child.release()));

  return OK;
}

scoped_ptr<Object> Directory::UnlinkChild(const std::string& name) {
  if (ChildWithName(name) == NULL)
    return scoped_ptr<Object>();

  scoped_ptr<Object> child(ChildWithName(name));
  children_.erase(name);

  child->UnlinkFromParent(this);

  return child.Pass();
}

ScopedVector<Object> Directory::TakeChildren() {
  ScopedVector<Object> taken;

  std::vector<Object*> children_to_take = children();
  for (std::vector<Object*>::iterator i(children_to_take.begin());
       i != children_to_take.end(); ++i) {
    taken.push_back(UnlinkChild((*i)->name()).release());
  }
  return taken.Pass();
}

// static
template <class T>
Status Directory::CreateObject(Directory* where, const std::string& name,
                               T** result) {
  DCHECK(where);

  scoped_ptr<T> new_one(new T());
  T* ret = new_one.get();

  DO_OR_RETURN(new_one->RenameTo(name));
  DO_OR_RETURN(where->LinkChild(scoped_ptr<Object>(new_one.Pass())));

  if (result)
    *result = ret;

  return OK;
}

} // namespace vfs
} // namespace fileman
