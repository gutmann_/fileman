#ifndef VFS_FILESYSTEM_H_
#define VFS_FILESYSTEM_H_

#include "fileman/vfs/directory.h"
#include "fileman/vfs/lock/lock_set.h"

namespace fileman {

namespace session {
class User;
} // namespace session

namespace vfs {

/// Filesystem object
class Filesystem {
 public:
  Filesystem();

  /// Find object by it pah
  /// @param path -- path to object. *MUST* be absolute
  /// @return pointer to found object or NULL if object not found
  /// @{
  Object* GetObject(base::FilePath path);
  const Object* GetObject(base::FilePath path) const;
  /// @}

  /// Create lock on object by specified user
  /// @param what -- locking object
  /// @param who -- user who performs lock
  lock::Status CreateLock(Object* what, const session::User& who);

  /// Remove lock on object
  /// @param what -- locked object
  /// @param who -- who performs unlock
  lock::Status RemoveLock(Object* what, const session::User& who);

  const lock::LockSet& lock_set() const;
 private:
  Directory root_;
  lock::LockSet fs_locks_;
};

} // namespace vfs
} // namespace fileman

#endif // VFS_FILESYSTEM_H_
