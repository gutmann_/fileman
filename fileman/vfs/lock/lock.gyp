# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'lock',
      'type': '<(component)',
      'sources': [
        'lock.cc',
        'lock.h',
        'lock_set.cc',
        'lock_set.h',
      ],
      'dependencies': [
        '../../base/base.gyp:base',
      ],
    },
  ],
}
