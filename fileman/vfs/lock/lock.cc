#include "fileman/vfs/lock/lock.h"

#include "fileman/base/logging.h"

namespace fileman {
namespace vfs {
namespace lock {
namespace __lock_internal {

LockBase::LockBase(const Object* what) : what_(what) { DCHECK(what); }

bool LockBase::operator <(const LockBase& that) const {
  return what_ < that.what_;
}

} // namespace __lock_internal
} // namespace lock
} // namespace vfs
} // namespace fileman
