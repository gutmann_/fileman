#ifndef VFS_LOCK_LOCK_SET_H_
#define VFS_LOCK_LOCK_SET_H_

#include "fileman/vfs/lock/lock.h"

#include <set>

namespace fileman {
namespace vfs {

class Directory;
class Object;

namespace lock {

/// Set of locks on filesystem objects.
class LockSet {
 public:
  /// Create lock on object by specified user
  /// @param what -- locking object
  /// @param who -- user who performs lock
  Status CreateLock(Object* what, const session::User* who);

  /// Remove lock on object
  /// @param what -- locked object
  /// @param who -- who performs unlock
  Status RemoveLock(Object* what, const session::User* who);

  /// Check that object is locked by specified user
  bool ObjectIsLockedBy(const Object* what, const session::User* who) const;

  /// List of users that locked object
  std::set<const session::User*> WhoLockedObject(const Object* what) const;

  /// Check that object contains any locked children
  /// NOTE: method doesn't check lock on |object|
  bool ObjectContainsLockedChildren(const Object* object) const;
 private:
  Status UnlockObject(Object* what, const session::User* who);

  template <class LockType>
  static LockType& FindOrCreateLock(Object* for_object,
                                    std::set<LockType>* where);
  template <class LockType>
  static Status ReleaseLock(Object* for_object, const session::User* by_user,
                            std::set<LockType>* where);


  std::set<Lock> object_locks_;
};

} // namespace lock
} // namespace vfs
} // namespace fileman

#endif // VFS_LOCK_LOCK_SET_H_
