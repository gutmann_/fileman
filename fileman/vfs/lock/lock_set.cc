#include "fileman/vfs/lock/lock_set.h"

#include "fileman/vfs/object.h"
#include "fileman/vfs/directory.h"
#include "fileman/vfs/file.h"
#include "fileman/vfs/visitor.h"

#include "fileman/base/logging.h"

namespace fileman {
namespace vfs {
namespace lock {

Status LockSet::CreateLock(Object* what, const session::User* who) {
  DCHECK(what);
  DCHECK(who);

  Lock& lock = FindOrCreateLock(what, &object_locks_);
  return lock.LockBy(who);
}

Status LockSet::RemoveLock(Object* what, const session::User* who) {
  return UnlockObject(what, who);
}

bool LockSet::ObjectIsLockedBy(const Object* what, 
                               const session::User* who) const {
  DCHECK(what);
  DCHECK(who);
  
  std::set<Lock>::const_iterator i = object_locks_.find(Lock(what));
  if (i != object_locks_.end())
    return i->LockedBy(who);
  return false;
}

std::set<const session::User*>
    LockSet::WhoLockedObject(const Object* what) const {
  DCHECK(what);

  std::set<Lock>::const_iterator i = object_locks_.find(Lock(what));
  if (i != object_locks_.end())
    return i->owners();
  return std::set<const session::User*>();
}

bool LockSet::ObjectContainsLockedChildren(const Object* object) const {
  class LockFinder : public ConstVisitor {
   public:
    LockFinder(const LockSet* ls) : ls_(ls), was_found_(false) {}

    virtual bool ShouldEnter(const Directory* into)      { return !was_found_; }
    virtual bool ShouldVisitChildren(const Directory* d) { return !was_found_; }

    virtual void OnEnter(const File* file) {
      if (!was_found_)
        was_found_ = !ls_->WhoLockedObject(file).empty();
    }

    bool was_found() const { return was_found_; }
   private:
    const LockSet* const ls_;
    bool was_found_;
  };

  LockFinder finder(this);
  object->Visit(&finder);
  return finder.was_found();
}

Status LockSet::UnlockObject(Object* what, const session::User* who) {
  DCHECK(what);
  DCHECK(who);

  return ReleaseLock(what, who, &object_locks_);
}

// static
template <class LockType>
LockType& LockSet::FindOrCreateLock(Object* for_object,
                                    std::set<LockType>* where) {
  DCHECK(for_object);
  DCHECK(where);

  typedef typename std::set<LockType>::iterator iterator;
  std::pair<iterator, bool> r = where->insert(LockType(for_object));
  return const_cast<LockType&>(*r.first);
}

// static
template <class LockType>
Status LockSet::ReleaseLock(Object* for_object, const session::User* by_user,
                            std::set<LockType>* where) {
  DCHECK(for_object);
  DCHECK(by_user);
  DCHECK(where);

  typedef typename std::set<LockType>::iterator iterator;
  iterator i = where->find(LockType(for_object));
  if (i == where->end()) {
    return NOT_LOCKED_BY_USER;
  }

  LockType* lock = const_cast<LockType*>(&*i);

  Status unlock_status = lock->UnlockBy(by_user);
  if (lock->ref_count() == 0) {
    lock = NULL;
    where->erase(i);
  }

  return unlock_status;
}

} // namespace lock
} // namespace vfs
} // namespace fileman
