#ifndef VFS_LOCK_LOCK_H_
#define VFS_LOCK_LOCK_H_

#include <cstddef>
#include <set>

namespace fileman {

namespace util {
namespace templates {

template <bool, typename T, typename U> struct if_ { typedef T type; };
template <typename T, typename U>
struct if_<false, T, U> { typedef U type; };


}
}

namespace session {
class User;
};

namespace vfs {

class Object;


namespace lock {

enum Status {
  OK,
  NOT_LOCKED_BY_USER,
  ALREADY_LOCKED_BY_USER,
  UNKNOWN_ERROR,
};

namespace __lock_internal {

class LockBase {
 public:
  explicit LockBase(const Object* what);
  
  // Required for some STL containers and operations
  bool operator <(const LockBase& that) const;
 private:
  const Object* const what_;
};

template <bool multi>
class Lock : public LockBase {
 public:
  explicit Lock(const Object* what)
    : LockBase(what),
      who_() {}

  bool LockedBy(const session::User* user) const {
    return who_.find(user) != who_.end();
  }

  Status LockBy(const session::User* user) {
    Status can_be_locked_status =
      EnsureCanLockedBy(user);
    if (can_be_locked_status != OK)
      return can_be_locked_status;

    who_.insert(user);
    return OK;
  }

  Status UnlockBy(const session::User* user) {
    typename set_t::iterator i = who_.find(user);
    
    if (who_.find(user) == who_.end()) {
      return NOT_LOCKED_BY_USER;
    } else {
      who_.erase(i);
      return OK;
    }
  }

  size_t ref_count() const { return who_.size(); }
  std::set<const session::User*> owners() const {
    return std::set<const session::User*>(who_.begin(), who_.end());
  }
 private:
  typedef typename util::templates::if_<multi,
            std::multiset<const session::User*>,
            std::set<const session::User*> // else
          >::type set_t;


  Status EnsureCanLockedBy(const session::User* user) const {
    return EnsureCanLockedBy(user, who_);
  }

  static Status EnsureCanLockedBy(const session::User* user,
                                  const std::set<const session::User*>& s) {
    if (s.find(user) != s.end()) {
      return ALREADY_LOCKED_BY_USER;
    }
    return OK;
  }

  static Status EnsureCanLockedBy(const session::User* user,
                                  const std::multiset<const session::User*>& s) {
    return OK;
  }


  set_t who_;
};

} // namespace __lock_internal

typedef __lock_internal::Lock<true>  ReentrantLock;
typedef __lock_internal::Lock<false> Lock;

} // namespace lock
} // namespace vfs
} // namespace fileman

#endif // VFS_LOCK_LOCK_H_
