#ifndef VFS_DIRECTORY_H_
#define VFS_DIRECTORY_H_

#include "fileman/vfs/object.h"

#include "fileman/base/memory/scoped_vector.h"

#include <map>

namespace fileman {
namespace vfs {

class File;

class Directory : public Object {
 public:
  static const base::FilePath ROOT_PATH;

  Directory();
  virtual ~Directory();

  /// Create empty file with name \p name
  /// @param name of new file
  /// @param pointer for result
  static Status CreateFile(Directory* where, const std::string& name,
                           File** result);

  /// Create empty directory with name \p name
  /// @param name of new directory
  /// @param result pointer to created directory. If error occured it will be
  ///         filled with NULL
  static Status CreateDirectory(Directory* where, const std::string& name,
                                Directory** result);

  /// Create object of type \p type
  /// @param where new object has been created
  /// @param name of new object
  /// @param type of new object
  /// @param result pointer to place newly created object
  static Status CreateObject(Directory* where, const std::string& name,
                             Object::Type type, Object** result = NULL);

  /// Link child (and it's children if exists) to this directory
  Status LinkChild(scoped_ptr<Object> child);

  /// Unlink child (and it's children if exists) from this directory
  /// @return pointer to unlinked child or NULL if child doesn't exists
  scoped_ptr<Object> UnlinkChild(const std::string& name);

  /// Unlink children from directory
  /// @return unlinked children
  ScopedVector<Object> TakeChildren();

  /// Find child with \p name
  /// @param name
  /// @return Object with \p name or NULL if not found
  /// @{
  Object* ChildWithName(const std::string& name);
  const Object* ChildWithName(const std::string& name) const;
  /// @}


  /// Clone this directory with subdirectories and files
  virtual scoped_ptr<Object> Clone() const;

  /// Visit this directory and it's subdirectories and files
  /// using |visitor|
  /// @{
  virtual void Visit(Visitor* visitor);
  virtual void Visit(ConstVisitor* visitor) const;
  /// @}

  /// Directory is empty?
  /// @return true if directory is empty and false otherwise
  bool Empty() const;

  /// List of children nodes of directory.
  /// @{
  std::vector<Object*> children();
  std::vector<const Object*> children() const;
  /// @}
 protected:
  /// We are forbid implicit copying...
  Directory(const Directory& src);
 private:
  Status EnsureChildExists(const std::string& name) const;
  Status EnsureChildNotExists(const std::string& name) const;

  template <class T>
  static Status CreateObject(Directory* where, const std::string& name,
                             T** result);


  std::map<std::string, Object*> children_;
};

} // namespace vfs
} // namespace fileman

#endif // VFS_OBJECT_H_
