# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'vfs',
      'type': '<(component)',
      'sources': [
        'directory.cc',
        'directory.h',
        'file.cc',
        'file.h',
        'filesystem.cc',
        'filesystem.h',
        'object.cc',
        'object.h',
        'visitor.h',

        'vfs_util.cc',
        'vfs_util.h',
      ],
      'dependencies': [
        '../base/base.gyp:base',
        'lock/lock.gyp:lock',
      ],
    },
  ],
}
