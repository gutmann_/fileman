#include "fileman/vfs/object.h"
#include "fileman/vfs/directory.h"

#include "fileman/base/logging.h"
#include "fileman/base/strings/string_util.h"

#include <algorithm>

namespace fileman {
namespace vfs {

Object::Object(Type type)
  : type_(type),
    name_(),
    parent_(NULL) {}

Object::Object(const Object& src) 
  : type_(src.type()),
    name_(src.name()),
    parent_(NULL) {}

Object::~Object() {}

Status Object::RenameTo(const std::string& to_name) {
  if (parent()) {
    if (base::FilePath(to_name) == Directory::ROOT_PATH)
      return BAD_NAME;
  }

  name_ = to_name;
  return OK;
}

void Object::GetPathComponents(std::vector<std::string>* to) const {
  DCHECK(to);
  to->clear();

  to->push_back(name());
  for (const Directory* p = parent(); p != NULL; p = p->parent())
    to->push_back(p->name());

  std::reverse(to->begin(), to->end());
}

const std::string& Object::name() const { return name_; }
Object::Type Object::type() const { return type_; }

Directory*       Object::parent()       { return parent_; }
const Directory* Object::parent() const { return parent_; }

base::FilePath Object::path() const {
  std::vector<std::string> path_components;
  GetPathComponents(&path_components);
  if (parent()) {
    path_components.front().clear(); // replace root name with empty string
                                     // to avoid separator duplicate at path
  }
 
  return base::FilePath(JoinString(path_components,
                                   base::FilePath::kSeparators[0]));
}

void Object::LinkWithParent(Directory* parent) {
  DCHECK(parent_ != parent); // we doesn't allow multiple bind to one parent
  DCHECK(parent);            // and to NULL parent

  parent_ = parent;
}

void Object::UnlinkFromParent(Directory* parent) {
  DCHECK(parent_ == parent); // we can unbind onlybinded parent

  parent_ = NULL;
}



std::string ObjectTypeAsString(Object::Type type) {
  std::vector<std::string> types;

  if (static_cast<int>(type) & static_cast<int>(Object::kFile))
    types.push_back("file");
  if (static_cast<int>(type) & static_cast<int>(Object::kDirectory))
    types.push_back("directory");

  return JoinString(types, ",");
}

} // namespace vfs
} //namespace fileman
