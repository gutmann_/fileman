#ifndef VFS_FILE_H_
#define VFS_FILE_H_

#include "fileman/vfs/object.h"

namespace fileman {
namespace vfs {

class File : public Object {
 public:
  File();
  virtual ~File();

  virtual scoped_ptr<Object> Clone() const;

  virtual void Visit(Visitor* visitor);
  virtual void Visit(ConstVisitor* visitor) const;
 protected:
  File(const File& src);
};

} // namespace vfs
} // namespace fileman

#endif // VFS_FILE_H_
