#include "fileman/vfs/filesystem.h"

#include "fileman/vfs/file.h"
#include "fileman/vfs/visitor.h"

#include "fileman/base/logging.h"
#include "fileman/base/strings/string_util.h"

#include <algorithm>
#include <string>
#include <vector>

namespace fileman {
namespace vfs {

namespace {

class FindObject : public Visitor {
 public:
  FindObject(const std::vector<std::string>& path);

  virtual bool ShouldEnter(const Directory* into);
  virtual bool ShouldVisitChildren(const Directory* d);

  virtual void OnEnter(File* f);
  virtual void OnEnter(Directory* d);

  Object* found() const;
 private:
  std::vector<std::string> curr_path_;
  Object* found_;

  const std::string path_;
};

FindObject::FindObject(const std::vector<std::string>& path)
  : curr_path_(path),
    found_(NULL),
    path_(JoinString(path, base::FilePath::kSeparators[0])) {
  std::reverse(curr_path_.begin(), curr_path_.end());
}

bool FindObject::ShouldEnter(const Directory* into) {
  return !found_ && !curr_path_.empty() &&
         curr_path_.back() == into->name();
}

bool FindObject::ShouldVisitChildren(const Directory* d) {
  return false; // we'll be visiting children "by hand" (see code below)
}

void FindObject::OnEnter(File* f) {
  if (f->name() == curr_path_.back()) {
    curr_path_.pop_back();

    if (curr_path_.empty()) {
      found_ = f;
    } else {
      DVLOG(3) << path_ << " is not a directory";
    }
  }
}

void FindObject::OnEnter(Directory* d) {
  DCHECK(d->name() == curr_path_.back());
  DVLOG(3) << "Entering to " << d->name();

  curr_path_.pop_back();

  if (curr_path_.empty()) {
    found_ = d;
  } else {
    Object* c = d->ChildWithName(curr_path_.back());
    if (c != NULL) {
      c->Visit(this);
    } else {
      DVLOG(3) << path_ << " not found";
    }
  }
}

Object* FindObject::found() const { return found_; }

} // anonimous namespace

Filesystem::Filesystem()
  : root_(),
    fs_locks_() {}

Object* Filesystem::GetObject(base::FilePath path) {
  DCHECK(path.IsAbsolute());

  std::vector<std::string> path_components;
  path.GetComponents(&path_components);

  FindObject find_object(path_components);
  root_.Visit(&find_object);
  return find_object.found();
}

const Object* Filesystem::GetObject(base::FilePath path) const {
  Filesystem* nonconst_this = const_cast<Filesystem*>(this);
  return nonconst_this->GetObject(path);
}

lock::Status Filesystem::CreateLock(Object* what, const session::User& who) {
  return fs_locks_.CreateLock(what, &who);
}

lock::Status Filesystem::RemoveLock(Object* what, const session::User& who) {
  return fs_locks_.RemoveLock(what, &who);
}

const lock::LockSet& Filesystem::lock_set() const {
  return fs_locks_;
}

} // namespace vfs
} // namespace fileman
