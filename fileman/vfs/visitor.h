#ifndef VFS_VISITOR_H_
#define VFS_VISITOR_H_

namespace fileman {
namespace vfs {

#define VISITOR_ENTER_LEAVE_PAIR(Type) \
  virtual void OnEnter(Type* o) {} \
  virtual void OnLeave(Type* o) {}

class File;
class Directory;

class VisitorBase {
 public:
  virtual ~VisitorBase() {}

  virtual bool ShouldEnter(const Directory* into)      { return true; }
  virtual bool ShouldVisitChildren(const Directory* d) { return true; }
};

class Visitor : public VisitorBase {
 public:
  VISITOR_ENTER_LEAVE_PAIR(File);
  VISITOR_ENTER_LEAVE_PAIR(Directory);
};

class ConstVisitor : public VisitorBase {
 public:
  VISITOR_ENTER_LEAVE_PAIR(const File);
  VISITOR_ENTER_LEAVE_PAIR(const Directory);
};

} // namespace vfs
} // namespace fileman

#endif // VFS_VISITOR_H_
