#ifndef VFS_OBJECT_H_
#define VFS_OBJECT_H_

#include "fileman/base/files/file_path.h"
#include "fileman/base/memory/scoped_ptr.h"

#include <string>
#include <vector>

namespace fileman {
namespace vfs {

class Directory;

class Visitor;
class ConstVisitor;

enum Status {
  OK,
  OBJECT_EXISTS,
  OBJECT_NOT_EXISTS,
  DIRECTORY_NOT_EMPTY,
  UNLINK_ERROR,
  BAD_NAME,
  UNKNOWN_ERROR,
};

/// Generic file object.
class Object {
 friend class Directory;
 public:
  enum Type {
    kFile      = 1,
    kDirectory = 2,
    kBadObject = 0,
  };

  Object(Type type);
  Object(const Object& src);
  virtual ~Object() = 0;

  /// Rename file object
  Status RenameTo(const std::string& to_name);

  /// Get full path components into \p to vector
  /// @param to pointer to vector which used to store result
  void GetPathComponents(std::vector<std::string>* to) const;

  /// Performs deep clone of this object
  /// @return scoped pointer to cloned object
  virtual scoped_ptr<Object> Clone() const = 0;

  virtual void Visit(Visitor* visitor) = 0;
  virtual void Visit(ConstVisitor* visitor) const = 0;

  const std::string& name() const;
  Type type() const;

  Directory* parent();
  const Directory* parent() const;

  base::FilePath path() const;
 protected:
  void LinkWithParent(Directory* parent);
  void UnlinkFromParent(Directory* parent);
 private:
  const Type type_;
  std::string name_;

  Directory* parent_;
};

std::string ObjectTypeAsString(Object::Type type);

} // namespace vfs
} // namespace fileman

#endif // VFS_OBJECT_H_
