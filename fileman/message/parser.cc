#include "fileman/message/parser.h"

#include "fileman/message/message.h"

#include "fileman/base/logging.h"
#include "fileman/base/strings/string_tokenizer.h"

namespace fileman {
namespace message {

bool Parser::Parse(const std::string& str, Message* into) {
  DCHECK(into);

  base::StringTokenizer t(str, " \t");
  if (!t.GetNext()) {
    DVLOG(3) << "Not enough tokens";
    return false;
  } else {
    Opcode opcode = OpcodeFromStr(t.token());
    if (opcode == UNKNOWN) return false;
    into->Reset(opcode);
  }

  while (t.GetNext())
    into->PutArg(t.token());
  return true;
}

} // namespace message
} // namespace fileman
