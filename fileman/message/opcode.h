#ifndef MESSAGE_OPCODE_H_
#define MESSAGE_OPCODE_H_

#include <string>

namespace fileman {
namespace message {

/// Operation codes
/// Warning: this numbers must be (exaclty!) continuous numbers
enum Opcode {
  QUIT    = 0x01,
  MD      = 0x02,
  CD      = 0x03,
  RD      = 0x04,
  DELTREE = 0x05,
  MF      = 0x06,
  DEL     = 0x07,
  LOCK    = 0x08,
  UNLOCK  = 0x09,
  COPY    = 0x0a,
  MOVE    = 0x0b,
  PRINT   = 0x0c,
  AUTH    = 0x0d,

  NOTIFY      = 0x0e,
  EXEC_STATUS = 0x0f,
  UNKNOWN,
};

/// Convert integer to opcode
/// @return Opcode or UNKNOWN opcode if num not match to
///         any known opcode
Opcode OpcodeFromNum(int num);

/// Convert string to opcode
/// @return Opcode or UNKNOWN opcode if string not match to
///         any known opcode
Opcode OpcodeFromStr(const std::string& str);

/// Convert opcode to it string representation.
std::string OpcodeToStr(Opcode opcode);

/// Opcode is command?
bool OpcodeIsCommand(Opcode opcode);

/// Opcode is notification?
bool OpcodeIsNotification(Opcode opcode);

/// Opcode can be executed or it's just notification message?
bool CouldBeExecuted(Opcode opcode);

} // namespace message
} // namespace fileman

#endif // MESSAGE_OPCODE_H_
