// Copyright 2014 Google Inc. All rights reserved.
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file or at
// https://developers.google.com/open-source/licenses/bsd

#include "fileman/message/buffer_writer.h"

#include "fileman/base/sys_byteorder.h"

namespace fileman {
namespace message {

BufferWriter::BufferWriter() {
  const size_t kDefaultReservedCapacity = 0x40000;  // 256KB.
  buf_.reserve(kDefaultReservedCapacity);
}
BufferWriter::BufferWriter(size_t reserved_size_in_bytes) {
  buf_.reserve(reserved_size_in_bytes);
}
BufferWriter::~BufferWriter() {}

void BufferWriter::AppendInt(uint8_t v) {
  buf_.push_back(v);
}
void BufferWriter::AppendInt(uint16_t v) {
  AppendInternal(base::HostToNet16(v));
}
void BufferWriter::AppendInt(uint32_t v) {
  AppendInternal(base::HostToNet32(v));
}
void BufferWriter::AppendInt(uint64_t v) {
  AppendInternal(base::HostToNet64(v));
}
void BufferWriter::AppendInt(int16_t v) {
  AppendInternal(base::HostToNet16(v));
}
void BufferWriter::AppendInt(int32_t v) {
  AppendInternal(base::HostToNet32(v));
}
void BufferWriter::AppendInt(int64_t v) {
  AppendInternal(base::HostToNet64(v));
}

void BufferWriter::AppendNBytes(uint64_t v, size_t num_bytes) {
  DCHECK_GE(sizeof(v), num_bytes);
  v = base::HostToNet64(v);
  const uint8_t* data = reinterpret_cast<uint8_t*>(&v);
  AppendArray(&data[sizeof(v) - num_bytes], num_bytes);
}

void BufferWriter::AppendVector(const std::vector<uint8_t>& v) {
  buf_.insert(buf_.end(), v.begin(), v.end());
}

void BufferWriter::AppendArray(const uint8_t* buf, size_t size) {
  buf_.insert(buf_.end(), buf, buf + size);
}

void BufferWriter::AppendBuffer(const BufferWriter& buffer) {
  buf_.insert(buf_.end(), buffer.buf_.begin(), buffer.buf_.end());
}

template <typename T>
void BufferWriter::AppendInternal(T v) {
  AppendArray(reinterpret_cast<uint8_t*>(&v), sizeof(T));
}

}  // namespace message
}  // namespace fileman
