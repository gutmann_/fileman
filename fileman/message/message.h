#ifndef MESSAGE_MESSAGE_H_
#define MESSAGE_MESSAGE_H_

#include "fileman/message/opcode.h"

#include <stdint.h>

#include <string>
#include <vector>

namespace fileman {
namespace message {

class BufferReader;
class BufferWriter;

/// Message that client can send to server
/// and server can reply on it using same (this) class...
/// More formally: every interaction with client and server
/// produced by sending messages.
///
/// Message format:
///  0                   1                   2                   3
///  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// |           Size                |       Sequence number         |
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// |    Opcode     |  Error code   | Arguments...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ...             | Token (String)                              ...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ...                                                             |
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///
///  Size:
///    size of all message (except this 2 bytest)
///  Sequence number:
///    Sequence number of message
///  Opcode:
///    Code of operation
///  Error code:
///    Code of error (has the meaning only in replies)
///  Arguments:
///    List of strings with message arguments.
///  Token:
///    Information using to identify message sender.
///    Token encoded using the same method as for a string (see below)
///
///
/// Arguments format:
///  0                   1                   2                   3
///  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// |    Argc       | String...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///
///  Args:
///    Number of string in arguments.
///
/// String format:
///  0                   1                   2                   3 
///  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// |     Length    | Chars...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ...
/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///
///  Length:
///    Length of charactesrs
///  Chars:
///    Sequence of characters
class Message {
 public:
  enum Status {
    kOK,
    kNotEnoughBytes,
    kReadError,
  };

  Message();
  Message(Opcode opcode);

  /// Construct response message to |message|
  /// @param message for which reply will be constructed
  /// @param err_code -- code of error
  /// @param exec_result -- string with result of command execution
  static Message MakeResponseTo(const Message& message, int err_code,
                                const std::string& exec_result);
  /// Construct info message
  /// @param msg -- text of info message
  static Message InfoMessage(const std::string& msg = "");

  /// Read message from |reader|
  /// @return true on success and false otherwise
  Status ReadFrom(BufferReader* reader);

  /// Write message to |writer|
  /// @return true on success and false otherwise
  void WriteTo(BufferWriter* writer) const;

  /// Reset messagage (clear all fields) and set it opcode to |opcode|
  void Reset(Opcode opcode = UNKNOWN);

  void SetSequenceNumber(uint16_t seqnum);
  void SetErrorCode(uint8_t err_code);
  void PutArg(const std::string& arg);
  void SetToken(const std::vector<uint8_t>& token);

  Opcode opcode() const;
  uint16_t sequence_number() const;
  uint8_t error_code() const;
  const std::vector<std::string>& args() const;
  const std::vector<uint8_t>& token() const;
  size_t size() const;

  std::string AsString() const;
  std::string ArgsAsString() const;
  std::string Sender() const;
 private:
  bool ReadOpcodeFrom(BufferReader* reader);
  bool ReadArgsFrom(BufferReader* reader);
  bool ReadArgFrom(BufferReader* reader);
  bool ReadTokenFrom(BufferReader* reader);

  void WriteOpcodeTo(BufferWriter* writer) const;
  void WriteArgsTo(BufferWriter* writer) const;
  static void WriteArg(const std::string& arg, BufferWriter* writer);
  void WriteTokenTo(BufferWriter* writer) const;

  Opcode opcode_;
  uint16_t sequence_number_;
  uint8_t error_code_;
  std::vector<std::string> args_;
  std::vector<uint8_t> token_;
};

} // namespace message
} // namespace fileman

#endif
