#include "fileman/message/message.h"

#include "fileman/message/buffer_reader.h"
#include "fileman/message/buffer_writer.h"

#include "fileman/base/numerics/safe_conversions.h"

#include <sstream>

namespace fileman {
namespace message {

Message::Message()
  : opcode_(UNKNOWN),
    sequence_number_(0),
    error_code_(0),
    args_(),
    token_() {}

Message::Message(Opcode opcode)
  : opcode_(opcode),
    sequence_number_(0),
    error_code_(0),
    args_(),
    token_() {}

// static
Message Message::InfoMessage(const std::string& msg) {
  Message notification(NOTIFY);
  if (!msg.empty())
    notification.PutArg(msg);
  return notification;
}

// static
Message Message::MakeResponseTo(const Message& message, int err_code,
                                const std::string& exec_result) {
  Message resp(EXEC_STATUS);
  resp.SetSequenceNumber(message.sequence_number());
  resp.SetErrorCode(err_code);
  resp.PutArg(exec_result);
  return resp;
}

Message::Status Message::ReadFrom(BufferReader* reader) {
  DCHECK(reader);

  Reset();

  uint16_t message_size;
  if (reader->Read2(&message_size)) {
    if (message_size == 0) return kReadError;
    if (!reader->HasBytes(message_size)) {
      DVLOG(3) << "Not enough bytes in input stream";
      return kNotEnoughBytes;
    }
  } else {
    return kNotEnoughBytes;
  }

  const bool read_success =
    reader->Read2(&sequence_number_) &&
    ReadOpcodeFrom(reader)           &&
    reader->Read1(&error_code_)      &&
    ReadArgsFrom(reader)             &&
    ReadTokenFrom(reader);

  if (!read_success) {
    DVLOG(3) << "Can't read message";
    return kReadError;
  }
  DCHECK(size() == message_size);
  return kOK;
}

void Message::WriteTo(BufferWriter* writer) const {
  DCHECK(writer);

  const size_t writer_pos_before_writing = writer->Size();

  writer->AppendInt(static_cast<uint16_t>(size()));
  writer->AppendInt(sequence_number_);
  
  WriteOpcodeTo(writer);
  writer->AppendInt(error_code_);

  WriteArgsTo(writer);
  WriteTokenTo(writer);

  const size_t writer_pos_after_writing = writer->Size();
  const size_t bytes_written = writer_pos_after_writing -
                               writer_pos_before_writing;
  // check that we written exactly the same number of bytes
  // as we put in |Size| field (without size of this field)
  DCHECK(bytes_written == size() + sizeof(uint16_t));
}

void Message::Reset(Opcode opcode) {
  opcode_ = opcode;
  sequence_number_ = 0;
  error_code_ = 0;
  args_.clear();
  token_.clear();
}

void Message::SetSequenceNumber(uint16_t seqnum) {
  sequence_number_ = seqnum;
}

void Message::SetErrorCode(uint8_t err_code) {
  error_code_ = err_code;
}

void Message::PutArg(const std::string& arg) {
  args_.push_back(arg);
}

void Message::SetToken(const std::vector<uint8_t>& token) {
  token_ = token;
}

Opcode Message::opcode() const { return opcode_; }
uint16_t Message::sequence_number() const { return sequence_number_; }
uint8_t Message::error_code() const { return error_code_; }
const std::vector<std::string>& Message::args() const { return args_; }
const std::vector<uint8_t>& Message::token() const { return token_; }

namespace {

size_t SizeOfArgumentsInBytes(const std::vector<std::string>& args) {
  size_t size = 0;
  for (std::vector<std::string>::const_iterator i(args.begin());
       i != args.end(); ++i) {
    size += sizeof(uint8_t) + // len of string field size
            i->length();
  }
  return size;
}

} // anonimous namespace

size_t Message::size() const {
  return
    sizeof(uint16_t) + // Sequence number
    sizeof(uint8_t)  + // Opcode
    sizeof(uint8_t)  + // Error code

    sizeof(uint8_t)                + // number of arguments
    SizeOfArgumentsInBytes(args()) + // sum of sizes of each argument

    sizeof(uint8_t) + token().size(); // token
}

std::string Message::AsString() const {
  std::ostringstream oss;
  oss << OpcodeToStr(opcode());
  if (error_code() != 0)
    oss << "(error #" << static_cast<int>(error_code()) << ")";

  if (!args().empty())
    oss << ' ' << ArgsAsString();
  return oss.str();
}

std::string Message::ArgsAsString() const {
  std::ostringstream oss;
  if (!args().empty()) {
    for (std::vector<std::string>::const_iterator i(args().begin());
         i != args().end() - 1; ++i)
      oss << *i << ' ';
    oss << args().back();
  }
  return oss.str();
}

std::string Message::Sender() const {
  return std::string(token().begin(), token().end());
}

bool Message::ReadOpcodeFrom(BufferReader* reader) {
  uint8_t opcode_num;
  if (reader->Read1(&opcode_num)) {
    opcode_ = OpcodeFromNum(opcode_num);
    return true;
  }
  return false;
}

bool Message::ReadArgsFrom(BufferReader* reader) {
  uint8_t argc;
  if (!reader->Read1(&argc)) {
    DVLOG(3) << "Can't read number of arguments";
    return false;
  }
  DVLOG(3) << "Argc == " << static_cast<int>(argc);

  for (uint8_t i = 0; i < argc; ++i) {
    if (!ReadArgFrom(reader)) {
      DVLOG(3) << "Can't read argument #" << static_cast<int>(i);
      return false;
    }
  }
  return true;
}

bool Message::ReadArgFrom(BufferReader* reader) {
  uint8_t arglen;
  if (!reader->Read1(&arglen)) {
    DVLOG(3) << "Can't read argument len";
    return false;
  }

  std::vector<uint8_t> arg;
  arg.reserve(arglen);
  if (!reader->ReadToVector(&arg, arglen)) {
    DVLOG(3) << "Can't read argument value (" << static_cast<int>(arglen)
             << " bytes)";
    return false;
  }

  std::string arg_str(arg.begin(), arg.end());
  args_.push_back(arg_str);
  return true;
}

bool Message::ReadTokenFrom(BufferReader* reader) {
  uint8_t token_len;
  if (!reader->Read1(&token_len)) {
    LOG(ERROR) << "Can't read token";
    return false;
  }

  token_.reserve(token_len);
  if (!reader->ReadToVector(&token_, token_len)) {
    DVLOG(3) << "Can't read messge token (" << static_cast<int>(token_len)
             << " bytes)";
    return false;
  }

  return true;
}

void Message::WriteOpcodeTo(BufferWriter* writer) const {
  uint8_t opcode_num = static_cast<uint8_t>(opcode());
  writer->AppendInt(opcode_num);
}

void Message::WriteArgsTo(BufferWriter* writer) const {
  uint8_t argc = base::checked_cast<uint8_t>(args().size());
  writer->AppendInt(argc);

  for (std::vector<std::string>::const_iterator i(args().begin());
       i != args().end(); ++i) {
    WriteArg(*i, writer);
  }
}

// static
void Message::WriteArg(const std::string& arg, BufferWriter* writer) {
  uint8_t arglen = base::checked_cast<uint8_t>(arg.size());
  DVLOG(3) << "Writing string \"" << arg << "\" (" 
           << static_cast<int>(arglen) << " bytes)";
  writer->AppendInt(arglen);
  writer->AppendArray(reinterpret_cast<const uint8_t*>(arg.data()), arglen);
}

void Message::WriteTokenTo(BufferWriter* writer) const {
  uint8_t tokenlen = base::checked_cast<uint8_t>(token_.size());
  writer->AppendInt(tokenlen);
  writer->AppendVector(token_);
}

} // namespace message
} // namespace fileman
