# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'message',
      'type': '<(component)',
      'sources': [
        'buffer_reader.cc',
        'buffer_reader.h',
        'buffer_writer.cc',
        'buffer_writer.h',
        'byte_queue.cc',
        'byte_queue.h',
        'message.cc',
        'message.h',
        'opcode.cc',
        'opcode.h',
        'parser.cc',
        'parser.h',
        'stream_parser.cc',
        'stream_parser.h',
      ],
      'dependencies': [
        '../base/base.gyp:base',
      ],
    },
  ],
}
