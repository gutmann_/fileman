#include "fileman/message/stream_parser.h"

#include "fileman/message/buffer_reader.h"

#include "fileman/base/logging.h"

namespace fileman {
namespace message {

void StreamParser::Push(const uint8_t* data, size_t size) {
  queue_.Push(data, size);

  const uint8_t* data_in_buffer;
  int data_in_buffer_size;

  queue_.Peek(&data_in_buffer, &data_in_buffer_size);
  if (data_in_buffer_size > 0) {
    BufferReader reader(data_in_buffer, data_in_buffer_size);
    
    const int bytes_consumed = ReadMessagesFrom(&reader);
    queue_.Pop(bytes_consumed);
  }
}

void StreamParser::OnMessage(const OnMessageCB& cb) {
  on_message_cb_ = cb;
}

void StreamParser::OnStreamParseError(const OnStreamParseErrorCB& cb) {
  on_stream_parse_error_cb_ = cb;
}

int StreamParser::ReadMessagesFrom(BufferReader* reader) {
  const size_t pos_before_reading = reader->pos();
  size_t pos = pos_before_reading;

  while (true) {
    Message message;

    const uint8_t* const begin_of_message = reader->data() + reader->pos();
    const Message::Status read_status = message.ReadFrom(reader);

    if (read_status == Message::kOK) {
      pos = reader->pos();
      on_message_cb_.Run(message);
    } else {
      if (read_status == Message::kNotEnoughBytes) {
        DVLOG(3) << "Not enough bytes to parse message.";
      } else {
        on_stream_parse_error_cb_.Run(read_status,
                                      std::vector<uint8_t>(begin_of_message,
                                                           reader->data() +
                                                           reader->pos()));
      }

      break;
    }
  }

  return pos - pos_before_reading;
}

} // namespace message
} // namespace fileman
