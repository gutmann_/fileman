#include "fileman/message/opcode.h"

#include <algorithm>

namespace {

const int kOpcodeMin = 0x01;
const int kOpcodeMax = 0x0f;

} // anonimous namespace

namespace fileman {
namespace message {

namespace {

const std::pair<std::string, Opcode> OPCODE_STR_TABLE[] = {
  std::make_pair("QUIT", QUIT),
  std::make_pair("MD", MD),
  std::make_pair("CD", CD),
  std::make_pair("RD", RD),
  std::make_pair("DELTREE", DELTREE),
  std::make_pair("MF", MF),
  std::make_pair("DEL", DEL),
  std::make_pair("LOCK", LOCK),
  std::make_pair("UNLOCK", UNLOCK),
  std::make_pair("COPY", COPY),
  std::make_pair("MOVE", MOVE),
  std::make_pair("PRINT", PRINT),
  std::make_pair("AUTH", AUTH),
  std::make_pair("NOTIFY", NOTIFY),
  std::make_pair("EXEC_STATUS", EXEC_STATUS),
};

const size_t OPCODE_STR_TABLE_SIZE = sizeof(OPCODE_STR_TABLE) /
                                     sizeof(OPCODE_STR_TABLE[0]);

const std::pair<std::string, Opcode>* OPCODE_STR_TABLE_BEGIN = OPCODE_STR_TABLE;
const std::pair<std::string, Opcode>* OPCODE_STR_TABLE_END =
  OPCODE_STR_TABLE + OPCODE_STR_TABLE_SIZE;


struct CompByStr {
  CompByStr(const std::string& s) : str_(s) {}

  bool operator ()(const std::pair<std::string, Opcode>& e) const {
    return e.first == str_;
  }

  const std::string& str_;
};

struct CompByOpcode {
  CompByOpcode(Opcode opcode) : opcode_(opcode) {}

  bool operator ()(const std::pair<std::string, Opcode>& e) const {
    return e.second == opcode_;
  }

  const Opcode opcode_;
};

} // anonimous namespace

Opcode OpcodeFromNum(int num) {
  if (kOpcodeMin <= num && num <= kOpcodeMax)
    return static_cast<Opcode>(num);
  return UNKNOWN;
}

Opcode OpcodeFromStr(const std::string& str) {
  std::string upper_case_str = str;
  std::transform(upper_case_str.begin(), upper_case_str.end(), upper_case_str.begin(),
                 ::toupper);

  const std::pair<std::string, Opcode>* found =
    std::find_if(OPCODE_STR_TABLE_BEGIN, OPCODE_STR_TABLE_END,
                 CompByStr(upper_case_str));

  if (found != OPCODE_STR_TABLE_END)
    return found->second;
  return UNKNOWN;
}

std::string OpcodeToStr(Opcode opcode) {
  const std::pair<std::string, Opcode>* found =
    std::find_if(OPCODE_STR_TABLE_BEGIN, OPCODE_STR_TABLE_END,
                 CompByOpcode(opcode));
  if (found != OPCODE_STR_TABLE_END)
    return found->first;
  return "UNKNOWN";
}

bool OpcodeIsCommand(Opcode opcode) {
  return QUIT <= opcode && opcode <= PRINT;
}

bool OpcodeIsNotification(Opcode opcode) {
  return opcode == NOTIFY || opcode == EXEC_STATUS;
}

bool CouldBeExecuted(Opcode opcode) {
  return MD <= opcode && opcode <= PRINT;
}

} // namespace message
} // namespace fileman
