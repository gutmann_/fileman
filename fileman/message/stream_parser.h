#ifndef MESSAGE_STREAM_PARSER_H_
#define MESSAGE_STREAM_PARSER_H_

#include "fileman/message/byte_queue.h"
#include "fileman/message/message.h"

#include "fileman/base/callback.h"

#include <vector>

namespace fileman {
namespace message {

class BufferReader;

/// Binary message stream parser
class StreamParser {
 public:
  typedef base::Callback<void (const Message& message)> OnMessageCB;
  typedef base::Callback<void (Message::Status error,
                               const std::vector<uint8_t>& msg)>
      OnStreamParseErrorCB;

  void Push(const uint8_t* data, size_t size);

  void OnMessage(const OnMessageCB& cb);
  void OnStreamParseError(const OnStreamParseErrorCB& cb);
 private:
  int ReadMessagesFrom(BufferReader* reader);

  ByteQueue queue_;

  OnMessageCB on_message_cb_;
  OnStreamParseErrorCB on_stream_parse_error_cb_;
};

} // namespace message
} // namespace fileman

#endif // MESSAGE_STREAM_PARSER_H_
