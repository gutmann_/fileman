#ifndef MESSAGE_PARSER_H_
#define MESSAGE_PARSER_H_

#include <string>

namespace fileman {
namespace message {

class Message;

/// A simple message parser.
class Parser {
 public:
  /// Parse message from string
  /// @param str a string to parse...
  /// @param into .. parse |into| message
  /// @return true on success and false otherwise
  bool Parse(const std::string& str, Message* into);
};

} // namespace message
} // namespace fileman

#endif // MESSAGE_PARSER_H_
