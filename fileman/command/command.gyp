# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'command',
      'type': '<(component)',
      'sources': [
        'executor.cc',
        'executor.h',
      ],
      'dependencies': [
        'commands/commands.gyp:commands',
        '../message/message.gyp:message',
        '../session/session.gyp:user',
        '../vfs/vfs.gyp:vfs',
        '../base/base.gyp:base',
      ],
    },
  ],
}
