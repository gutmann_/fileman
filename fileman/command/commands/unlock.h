#ifndef COMMAND_COMMANDS_UNLOCK_H_
#define COMMAND_COMMANDS_UNLOCK_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class Unlock : public Command {
 public:
  virtual ~Unlock();

  virtual std::vector<TargetFilter> TargetFilters() const;
 protected:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;
 private:
  virtual void DoExecute(vfs::Filesystem* fs,
                         const std::vector<Target>& targets);
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_LOCK_H_
