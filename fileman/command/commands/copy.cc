#include "fileman/command/commands/copy.h"

#include "fileman/command/commands/common.h"
#include "fileman/vfs/vfs_util.h"

#include "fileman/base/logging.h"

#include <iterator>

namespace fileman {
namespace command {

typedef Copy::TargetFilter TargetFilter;

Copy::~Copy() {}

std::vector<TargetFilter> Copy::TargetFilters() const {
  static TargetFilter filters[] = {
    TargetFilter
    (
     static_cast<vfs::Object::Type>(vfs::Object::kFile | vfs::Object::kDirectory),
     Command::kRead
    ),
    TargetFilter(vfs::Object::kDirectory, kWrite),
  };
  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath Copy::TargetPathForArg(const base::FilePath& arg,
                                      size_t arg_idx) const {
  return arg; // we will copy/write exactly specified targets
}

void Copy::DoExecute(vfs::Filesystem* fs, const std::vector<Target>& targets) {
  DCHECK(targets.size() == 2);
  using namespace vfs;
  
  const Object* src = targets.at(0).for_read();
  Object* dst = targets.at(1).for_write();

  scoped_ptr<Object> conflicts;
  Move(src->Clone(), dst, &conflicts, *fs);
  if (conflicts) {
    Return(1, "There are a conflicts (type mismatching and/or locks) "
              "while copying. Directory has been copied partially.");
  } else 
    Return(0);
}

} // namespace command
} // namespace fileman
