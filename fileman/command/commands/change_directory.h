#ifndef COMMAND_COMMANDS_CHANGE_DIRECTORY_H_
#define COMMAND_COMMANDS_CHANGE_DIRECTORY_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class ChangeDirectory : public Command {
 public:
  virtual ~ChangeDirectory();

  virtual std::vector<TargetFilter> TargetFilters() const;
 private:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;
  virtual void DoExecute(vfs::Filesystem* fs,
                         const std::vector<Target>& targets);
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_LOCK_H_
