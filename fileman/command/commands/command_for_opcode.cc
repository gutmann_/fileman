#include "fileman/command/commands/command.h"

#include "fileman/command/commands/copy.h"
#include "fileman/command/commands/change_directory.h"
#include "fileman/command/commands/lock.h"
#include "fileman/command/commands/make_object.h"
#include "fileman/command/commands/move.h"
#include "fileman/command/commands/print.h"
#include "fileman/command/commands/remove_object.h"
#include "fileman/command/commands/unlock.h"

namespace fileman {
namespace command {

using namespace message;

// static
scoped_ptr<Command> Command::ForOpcode(Opcode opcode) {
  using namespace vfs;

  switch (opcode) {
    case CD      : return scoped_ptr<Command>(new ChangeDirectory());
    case COPY    : return scoped_ptr<Command>(new Copy());
    case LOCK    : return scoped_ptr<Command>(new Lock());
    case DEL     : return scoped_ptr<Command>(new RemoveObject(Object::kFile, RemoveObject::kNormal));
    case DELTREE : return scoped_ptr<Command>(new RemoveObject(Object::kDirectory, RemoveObject::kRecursive));
    case MD      : return scoped_ptr<Command>(new MakeObject(Object::kDirectory));
    case MF      : return scoped_ptr<Command>(new MakeObject(Object::kFile));
    case MOVE    : return scoped_ptr<Command>(new Move());
    case PRINT   : return scoped_ptr<Command>(new Print());
    case RD      : return scoped_ptr<Command>(new RemoveObject(Object::kDirectory, RemoveObject::kNormal));
    case UNLOCK  : return scoped_ptr<Command>(new Unlock());
    default      : return scoped_ptr<Command>();
  }
}

} // namespace command
} // namespace fileman
