#ifndef COMMAND_COMMANDS_COPY_H_
#define COMMAND_COMMANDS_COPY_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class Copy : public Command {
 public:
  virtual ~Copy();

  virtual std::vector<TargetFilter> TargetFilters() const;
 protected:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;
 private:
  virtual void DoExecute(vfs::Filesystem* fs, const std::vector<Target>& targets);
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_COPY_H_
