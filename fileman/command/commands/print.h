#ifndef COMMAND_COMMANDS_PRINT_H_
#define COMMAND_COMMANDS_PRINT_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class Print : public Command {
 public:
  virtual ~Print();

  virtual std::vector<TargetFilter> TargetFilters() const;
 protected:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;
 private:
  virtual void DoExecute(vfs::Filesystem* fs,
                         const std::vector<Target>& targets);
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_PRINT_H_
