#include "fileman/command/commands/remove_object.h"

#include "fileman/command/commands/common.h"
#include "fileman/session/environment.h"
#include "fileman/vfs/directory.h"
#include "fileman/vfs/filesystem.h"
#include "fileman/vfs/vfs_util.h"
#include "fileman/vfs/lock/lock_set.h"

#include "fileman/base/logging.h"

#include <sstream>

namespace fileman {
namespace command {

typedef Command::TargetFilter TargetFilter;

RemoveObject::RemoveObject(vfs::Object::Type type, RemoveMode mode)
  : type_(type),
    mode_(mode) {}

RemoveObject::~RemoveObject() {}

std::vector<TargetFilter> RemoveObject::TargetFilters() const {
  TargetFilter filters[] = {
    TargetFilter(type_, kWrite),
  };

  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath RemoveObject::TargetPathForArg(const base::FilePath& arg,
                                              size_t arg_idx) const {
  return arg;
}

namespace {

class DeleteNotLocked : public vfs::UnaryPredicate {
 public:
  DeleteNotLocked(const vfs::lock::LockSet& ls) : ls_(ls) {}

  virtual bool operator ()(const vfs::Object* o) {
    using namespace vfs;

    bool need_to_delete = false;
    if (o->type() == Object::kFile) {
      need_to_delete = ls_.WhoLockedObject(o).empty();
    } else if (o->type() == Object::kDirectory) {
      const Directory* dir = static_cast<const Directory*>(o);
      need_to_delete = dir->children().empty();
    }

    DVLOG(3) << "Object \"" << o->path().value()
             << "\" will " << (need_to_delete ? "BE" : "NOT BE") << " deleted";
    return need_to_delete;
  }
 private:
  const vfs::lock::LockSet& ls_;
};

} // anonimous namespace

void RemoveObject::DoExecute(vfs::Filesystem* fs,
                             const std::vector<Target>& targets) {
  using namespace vfs;

  int err_code = 0;
  std::ostringstream error_message;

  FOREACH_ARG_AND_TARGET {
    Object* to_del = target->for_write();

    if (mode_ == kNormal && to_del->type() == Object::kDirectory) {
      const Directory* d = static_cast<Directory*>(to_del);
      if (!d->children().empty()) {
        err_code = 1;
        error_message << "Directory \"" << to_del->path().value() << "\" not empty";
        continue;
      }
    }

    if (to_del->path() == Environment()->CWD()) {
      err_code = 1;
      error_message << "can't remove current directory" << std::endl;
      continue;
    }

    vfs::Directory* parent = to_del->parent();
    const base::FilePath path_to_del = to_del->path();
    DCHECK(parent);
    scoped_ptr<Object> child = parent->UnlinkChild(to_del->name());

    DeleteNotLocked delete_not_locked(fs->lock_set());
    child = DeleteIf(child.Pass(), &delete_not_locked);
    if (child) {
      parent->LinkChild(child.Pass());

      err_code = 1;
      error_message << "Can't remove \"" << path_to_del.value()
                    << "\" because it's locked";
    }
  }

  Return(err_code, error_message.str());
}

} // namespace command
} // namespace fileman
