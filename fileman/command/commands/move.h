#ifndef COMMAND_COMMANDS_MOVE_H_
#define COMMAND_COMMANDS_MOVE_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class Move : public Command {
 public:
  virtual ~Move();

  virtual std::vector<TargetFilter> TargetFilters() const;
 protected:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;
 private:
  virtual void DoExecute(vfs::Filesystem* fs, const std::vector<Target>& targets);
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_MOVE_H_
