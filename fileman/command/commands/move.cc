#include "fileman/command/commands/move.h"

#include "fileman/command/commands/common.h"
#include "fileman/vfs/vfs_util.h"

#include "fileman/base/logging.h"

namespace fileman {
namespace command {

typedef Move::TargetFilter TargetFilter;

Move::~Move() {}

std::vector<TargetFilter> Move::TargetFilters() const {
  static TargetFilter filters[] = {
    TargetFilter
    (
     static_cast<vfs::Object::Type>(vfs::Object::kFile | vfs::Object::kDirectory),
     kWrite
    ),
    TargetFilter(vfs::Object::kDirectory, kWrite),
  };
  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath Move::TargetPathForArg(const base::FilePath& arg,
                                      size_t arg_idx) const {
  return arg; // we will move/write exactly specified targets
}

void Move::DoExecute(vfs::Filesystem* fs, const std::vector<Target>& targets) {
  DCHECK(targets.size() == 2);
  using namespace vfs;
  
  Object* _src = targets.at(0).for_write();
  Directory* src_parent = _src->parent();

  if (src_parent == NULL) {
    Return(1, "Can't move root directory");
    return;
  }

  Object* dst = targets.at(1).for_write();

  DVLOG(3) << "Moving \"" << _src->path().value() << "\" to \""
           << dst->path().value() << '\"';

  scoped_ptr<Object> src = src_parent->UnlinkChild(_src->name());

  scoped_ptr<Object> conflicts;
  vfs::Move(src.Pass(), dst, &conflicts, *fs);

  if (conflicts) {
    Return(1, "There are a conflicts (type mismatching and/or locks) "
              "while moving. Directory has been moved partially.");
    src_parent->LinkChild(conflicts.Pass());
  } else 
    Return(0);
}

} // namespace command
} // namespace fileman
