#ifndef COMMAND_COMMANDS_COMMON_H_
#define COMMAND_COMMANDS_COMMON_H_

#define FOREACH_ARG_AND_TARGET                                          \
  const std::vector<base::FilePath>& args = arguments();                \
                                                                        \
  std::vector<base::FilePath>::const_iterator arg(args.begin());        \
  std::vector<Target>::const_iterator target(targets.begin());          \
                                                                        \
  for (; arg != args.end() && target != targets.end(); ++arg, ++target)

#define RETURN(code, message) \
  Return(code, message);      \
  return;

#endif // COMMAND_COMMANDS_COMMON_H_
