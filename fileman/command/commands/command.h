#ifndef COMMAND_COMMANDS_H_
#define COMMAND_COMMANDS_H_

#include "fileman/message/opcode.h"
#include "fileman/vfs/object.h"

#include "fileman/base/callback.h"
#include "fileman/base/files/file_path.h"
#include "fileman/base/memory/scoped_ptr.h"

#include <vector>

namespace fileman {

namespace vfs {
class Filesystem;
} // namespace vfs

namespace session {
class Environment;
class User;
}

namespace command {

/// An abstract command which can be executed by this server
/// Algoright of command usage:
///  1. put param(s) to command using |PutArg| method
///  2. Generate targets using |Targets| method
///  3. Execute command on generated targets with |ExecuteOn| method
class Command {
 public:
  typedef base::Callback<void(int code, const std::string& message)>
      OnExecutedCB;

  /// Various target access requirements flags
  static const int kRead;
  static const int kWrite;
  static const int kNotLocked;

  /// This class encapsulates target and it's actual access mode
  /// and other access abilities
  class Target {
   public:
    Target(vfs::Object* for_write);
    Target(const vfs::Object* for_read);

    /// Return pointer to object for writing it or NULL if object
    /// accessible for a read only
    vfs::Object* for_write() const;

    /// Return pointer to object for reading it
    const vfs::Object* for_read() const;

    /// Return pointer to object stored in this target
    const vfs::Object* get() const;

    /// Access mode of target.
    /// @return bitset of |kRead| and/or |kWrite| flags
    int access_mode() const;
   private:
    vfs::Object* for_write_;
    const vfs::Object* for_read_;
  };

  /// Target filter. Need for list of targets validation
  /// (for example for write ability)
  class TargetFilter {
   public:
    /// @param object_type type of requirement object.
    /// @param requirements contains access requirements flags
    TargetFilter(vfs::Object::Type object_type, int requirements);

    /// Indicates that target need to be unlocked
    bool TargetNeedToBeNotLocked() const;

    /// Required acces mode
    /// @return bitset of |kRead| and/or |kWrite| flags
    int required_access_mode() const;
    /// Required object type
    vfs::Object::Type required_type() const;
   private:
    vfs::Object::Type object_type_;
    int requirements_; // bitset of all requirements
  };


  Command();
  virtual ~Command() = 0;

  /// Make command for corresponding opcode
  /// @param opcode of command
  /// @return pointer to command or NULL if there is no command for
  ///         provided opcode.
  static scoped_ptr<Command> ForOpcode(message::Opcode opcode);

  /// Generate targets for filesustem.
  /// @param fs is filesystem for operate on it
  /// @param targets is a pointer for store resulting targets
  /// @return true on success and false otherwise
  /// @{
  bool GetTargets(vfs::Filesystem* fs, std::vector<Target>* targets);
  bool GetTargets(const vfs::Filesystem* fs, std::vector<Target>* targets);
  ///@}

  /// Generate list of target filters for this command.
  virtual std::vector<TargetFilter> TargetFilters() const = 0;

  /// Execute command on specified filesystem and targets.
  /// @param fs -- filesystem
  /// @param targets -- list of targets (must be not empty)
  void ExecuteOn(vfs::Filesystem* fs, const std::vector<Target>& targets);

  /// Add argument for command arguments list
  void PutArg(const base::FilePath& arg);

  /// Specify which user will be execute command.
  /// @param who -- user who will be execute command
  void WillExecuteBy(const session::User& who);

  /// Specify the environment in which you run this command
  void WillExecutedIn(session::Environment* env);

  /// Specify callback for "on executed" event
  void OnExecuted(const OnExecutedCB& cb);

  /// Vector of arguments
  const std::vector<base::FilePath>& arguments() const;
 protected:
  /// Specifies path for target.
  /// Some commands read/write argument (like print)
  /// But other command need to read/write parent directory (like RD)
  /// @param arg of command
  /// @param arg_idx index of argument
  /// @return path to target
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const = 0;

  /// Return code and text message from command
  /// You can call |Return| multiple times but callback will receive
  /// *ONLY* first (code, message)
  /// @param code -- error code (there is an error if code != 0)
  /// @param message is the text message of error or result of execution
  void Return(int code, const std::string& message = "");

  const session::User* WhoExecuteThis() const;
  session::Environment* Environment() const;

  base::FilePath AbsPath(const base::FilePath& path) const;
  base::FilePath RelPath(const base::FilePath& path) const;
 private:
  virtual void DoExecute(vfs::Filesystem* fs,
                         const std::vector<Target>& targets) = 0;

  template <class FS>
  bool Match(const Target& target, const TargetFilter& filter, FS* fs);

  template <class FS>
  bool TargetsFor(FS* fs, std::vector<Target>* targets);


  std::vector<base::FilePath> args_;

  const session::User* user_;
  session::Environment* environment_;

  OnExecutedCB on_executed_cb_;
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_H_
