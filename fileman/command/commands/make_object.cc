#include "fileman/command/commands/make_object.h"

#include "fileman/command/commands/common.h"
#include "fileman/vfs/directory.h"

#include "fileman/base/logging.h"

#include <sstream>

namespace fileman {
namespace command {

typedef Command::TargetFilter TargetFilter;

MakeObject::MakeObject(vfs::Object::Type type)
  : type_(type) {}

MakeObject::~MakeObject() {}

std::vector<TargetFilter> MakeObject::TargetFilters() const {
  static TargetFilter filters[] = {
    TargetFilter(vfs::Object::kDirectory, Command::kWrite),
  };
  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath MakeObject::TargetPathForArg(const base::FilePath& arg,
                                            size_t arg_idx) const {
  return arg.DirName(); // we will create a file in it's parent
}

void MakeObject::DoExecute(vfs::Filesystem* fs,
                           const std::vector<Target>& targets) {
  int err_code = 0;
  std::ostringstream error_message;

  FOREACH_ARG_AND_TARGET {
    vfs::Directory* directory =
      static_cast<vfs::Directory*>(target->for_write());

    vfs::Status status =
      vfs::Directory::CreateObject(directory, arg->BaseName().value(), type_);
    if (status != vfs::OK) {
      err_code = 1;
      error_message << "can't create \"" << arg->BaseName().value()
                    << '\"' << std::endl;
    }
  }

  Return(err_code, error_message.str());
}

} // namespace command
} // namespace fileman
