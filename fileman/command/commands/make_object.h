#ifndef COMMAND_COMMANDS_MAKE_OBJECT_H_
#define COMMAND_COMMANDS_MAKE_OBJECT_H_

#include "fileman/command/commands/command.h"

namespace fileman {
namespace command {

class MakeObject : public Command {
 public:
  MakeObject(vfs::Object::Type type);
  virtual ~MakeObject();

  virtual std::vector<TargetFilter> TargetFilters() const;
 private:
  virtual base::FilePath TargetPathForArg(const base::FilePath& arg,
                                          size_t arg_idx) const;

  virtual void DoExecute(vfs::Filesystem* fs,
                         const std::vector<Target>& targets);

  const vfs::Object::Type type_;
};

} // namespace command
} // namespace fileman

#endif // COMMAND_COMMANDS_MAKE_OBJECT_H_
