#include "fileman/command/commands/print.h"

#include "fileman/session/user.h"
#include "fileman/vfs/directory.h"
#include "fileman/vfs/file.h"
#include "fileman/vfs/filesystem.h"
#include "fileman/vfs/visitor.h"
#include "fileman/vfs/lock/lock_set.h"

#include "fileman/base/logging.h"

#include <sstream>

namespace fileman {

namespace vfs {
namespace {

class PrintTreeVisitor : public ConstVisitor {
 public:
  PrintTreeVisitor(const Filesystem* fs);

  virtual void OnEnter(const File* file);
  virtual void OnLeave(const File* file);

  virtual void OnEnter(const Directory* dir);
  virtual void OnLeave(const Directory* dir);

  void PrintTo(std::ostringstream* out);
 private:
  void Print(const Object* o);
  std::string NameAndInfo(const Object* o) const;

  int level_;

  const Filesystem* const fs_;
  std::ostringstream* out_;
};

PrintTreeVisitor::PrintTreeVisitor(const Filesystem* fs) : level_(0), fs_(fs) {}

void PrintTreeVisitor::OnEnter(const File* file) { Print(file); ++level_; }
void PrintTreeVisitor::OnLeave(const File* file) { --level_; }

void PrintTreeVisitor::OnEnter(const Directory* dir) { Print(dir); ++level_; }
void PrintTreeVisitor::OnLeave(const Directory* dir) { --level_; }

void PrintTreeVisitor::PrintTo(std::ostringstream* out) {
  DCHECK(out);
  out_ = out;
}

void PrintTreeVisitor::Print(const Object* o) {
  DCHECK(o);
  if (!out_) return;

  for (int l = 0; l < level_; ++l)
    (*out_) << (l < level_ - 1 ? "| " : "|_");
  (*out_) << NameAndInfo(o) << std::endl;
}

std::string PrintTreeVisitor::NameAndInfo(const Object* o) const {
  std::ostringstream oss;
  oss << (o->name().empty() ? "/" : o->name());
  // oss << (o->type() == Object::kDirectory ? "<d>" : "<f>");

  const std::set<const session::User*> locked_by =
    fs_->lock_set().WhoLockedObject(o);

  if (!locked_by.empty())
    oss << "[LOCKED by ";

  size_t idx = 0;
  for (std::set<const session::User*>::const_iterator i(locked_by.begin());
       i != locked_by.end(); ++i, ++idx) {
    oss << (*i)->name() << ((idx < locked_by.size() - 1) ? ", " : "");
  }

  if (!locked_by.empty())
    oss << ']';
  return oss.str();
}

} // anonimous namespace
} // namespace vfs

namespace command {

typedef Print::TargetFilter TargetFilter;

Print::~Print() {}

std::vector<TargetFilter> Print::TargetFilters() const {
  const TargetFilter filters[] = {
    TargetFilter(vfs::Object::kDirectory, Command::kRead),
  };

  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath Print::TargetPathForArg(const base::FilePath& arg,
                                       size_t arg_idx) const {
  return arg;
}

void Print::DoExecute(vfs::Filesystem* fs,
                      const std::vector<Target>& targets) {
  const vfs::Object* o = targets.front().for_read();
  std::ostringstream result;
  vfs::PrintTreeVisitor printer(fs);
  printer.PrintTo(&result);

  o->Visit(&printer);
  Return(0, result.str());
}

} // namespace command
} // namespace fileman
