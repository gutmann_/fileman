#include "fileman/command/commands/lock.h"

#include "fileman/command/commands/common.h"
#include "fileman/vfs/object.h"
#include "fileman/vfs/filesystem.h"
#include "fileman/vfs/lock/lock.h"

#include "fileman/base/logging.h"

#include <sstream>

namespace fileman {
namespace command {

typedef Lock::TargetFilter TargetFilter;

Lock::~Lock() {}

std::vector<TargetFilter> Lock::TargetFilters() const {
  static TargetFilter filters[] = {
    TargetFilter
    (
     vfs::Object::kFile,
     Command::kWrite
    ),
  };
  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath Lock::TargetPathForArg(const base::FilePath& arg,
                                      size_t arg_idx) const {
  return arg; // we will lock exactly specified targets
}

void Lock::DoExecute(vfs::Filesystem* fs, const std::vector<Target>& targets) {
  DCHECK(WhoExecuteThis());

  int err_code = 0;
  std::ostringstream err;

  FOREACH_ARG_AND_TARGET {
    vfs::Object* o = target->for_write();
    vfs::lock::Status lock_status = fs->CreateLock(o, *WhoExecuteThis());
    if (lock_status != vfs::lock::OK) {
      err_code = 1;
      err << "Can't lock object \"" << o->path().value() << '\"' << std::endl;
    }
  }

  Return(err_code, err.str());
}

} // namespace command
} // namespace fileman
