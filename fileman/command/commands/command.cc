#include "fileman/command/commands/command.h"

#include "fileman/command/commands/common.h"

#include "fileman/session/environment.h"
#include "fileman/vfs/filesystem.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"
#include "fileman/base/strings/string_util.h"

#include <algorithm>
#include <iterator>
#include <sstream>

namespace {

void __EmptyReturn(int, const std::string&) {}

} // anonimous namespace

namespace fileman {
namespace command {

typedef Command::Target Target;;

// static
const int Command::kRead              = 1;
const int Command::kWrite             = 2;
const int ACCESS_MODE_MASK            = 3;

const int Command::kNotLocked         = 4;

Target::Target(vfs::Object* for_write)
  : for_write_(for_write),
    for_read_(NULL) {}

Target::Target(const vfs::Object* for_read)
  : for_write_(NULL),
    for_read_(for_read) {}

vfs::Object* Target::for_write() const      { return for_write_; }
const vfs::Object* Target::for_read() const {      return get(); }

const vfs::Object* Target::get() const {
  return for_write_ ? for_write_ : for_read_;
}

int Target::access_mode() const {
  int mode = 0;
  if (get() != NULL)
    mode |= kRead;
  if (for_write() != NULL)
    mode |= kWrite;
  return mode;
}


typedef Command::TargetFilter TargetFilter;

TargetFilter::TargetFilter(vfs::Object::Type object_type,
                           int requirements)
  : object_type_(object_type),
    requirements_(requirements) {}

bool TargetFilter::TargetNeedToBeNotLocked() const {
  return (requirements_ & kNotLocked) == kNotLocked;
}

int TargetFilter::required_access_mode() const {
  return requirements_ & ACCESS_MODE_MASK;
}

vfs::Object::Type TargetFilter::required_type() const { return object_type_; }



Command::Command()
  : args_(),
    on_executed_cb_(base::Bind(&__EmptyReturn)) {}

Command::~Command() {
  Return(1, "Unexpected state"); // this shold not happen (e.g. client should not
                                 // receive theese message)
}

bool Command::GetTargets(vfs::Filesystem* fs, std::vector<Target>* targets) {
  return TargetsFor(fs, targets);
}

bool Command::GetTargets(const vfs::Filesystem* fs, std::vector<Target>* targets) {
  return TargetsFor(fs, targets);
}

void Command::ExecuteOn(vfs::Filesystem* fs,
                        const std::vector<Target>& targets) {
  if (targets.size() < TargetFilters().size()) {
    std::ostringstream err;
    err << "You does not specify all required params! Excepted "
        << TargetFilters().size() << " but actually you've passed "
        << targets.size() << " params.";
    Return(1, err.str());
  } else {
    DoExecute(fs, targets);
  }
}

void Command::PutArg(const base::FilePath& arg) {
  args_.push_back(arg);
}

void Command::WillExecuteBy(const session::User& who) {
  user_ = &who;
}

void Command::WillExecutedIn(session::Environment* env) {
  environment_ = env;
}

const std::vector<base::FilePath>& Command::arguments() const { return args_; }

void Command::OnExecuted(const OnExecutedCB& cb) {
  on_executed_cb_ = cb;
}

void Command::Return(int code, const std::string& message) {
  on_executed_cb_.Run(code, message); // call the callback
  on_executed_cb_ = base::Bind(&__EmptyReturn); // and reset it to NOP-function
}

const session::User* Command::WhoExecuteThis() const {
  return user_;
}

session::Environment* Command::Environment() const {
  return environment_;
}

base::FilePath Command::AbsPath(const base::FilePath& path) const {
  if (path.IsAbsolute()) {
    return path;
  } else {
    DCHECK(Environment());
    base::FilePath abs_path = Environment()->CWD().Append(path);
    DCHECK(abs_path.IsAbsolute());
    return abs_path;
  }
}

base::FilePath Command::RelPath(const base::FilePath& path) const {
  if (!path.IsAbsolute()) {
    return path;
  } else {
    typedef std::vector<base::FilePath::StringType> path_components_t;
    path_components_t cwd_components, path_components;
    DCHECK(Environment());
    
    Environment()->CWD().GetComponents(&cwd_components);
    path.GetComponents(&path_components);

    path_components_t res_components;
    path_components_t::iterator i(cwd_components.begin()),
                                j(path_components.begin());
    // skip common path
    for (; i != cwd_components.end() && j != path_components.end(); ++i, ++j)
      if (*i != *j) break;
    // and copy rest of it
    std::copy(j, path_components.end(), std::back_inserter(res_components));

    return base::FilePath(JoinString(res_components, "/"));
  }
}

template <class FS>
bool Command::Match(const Target& target, const TargetFilter& filter,
                    FS* fs) {
  const bool type_is_ok = static_cast<int>(target.get()->type()) &
                          static_cast<int>(filter.required_type());

  const bool access_is_ok = target.access_mode() & filter.required_access_mode();

  if (!type_is_ok) {
    std::stringstream err;

    err << "Type mismatch! Excepted "
        << vfs::ObjectTypeAsString(filter.required_type())
        << ", but actual is "
        << vfs::ObjectTypeAsString(target.get()->type());
    Return(1, err.str());
    return false;
  }
  if (!access_is_ok) {
    std::stringstream err;
    err << "Access mode mismatch! Excepted "
        << filter.required_access_mode()
        << ", but actual is "
        << target.access_mode();
    Return(1, err.str());
    return false;
  }

  if (filter.TargetNeedToBeNotLocked()) {
    DVLOG(5) << "Lock is required so checking it...";
    const vfs::lock::LockSet& locks = fs->lock_set();
    if (!locks.WhoLockedObject(target.get()).empty() ||
        locks.ObjectContainsLockedChildren(target.get())) {
      std::stringstream err;
      err << "Object is locked";
      Return(1, err.str());
      return false;
    }
  }
  return true;
}

template <class FS>
bool Command::TargetsFor(FS* fs, std::vector<Target>* targets) {
  DCHECK(fs);
  DCHECK(targets);

  DVLOG(3) << "Creating targets for fs...";

  const std::vector<TargetFilter> filters = TargetFilters();
  if (args_.size() > filters.size()) {
    Return(1, "too few arguments");
    return false;
  }

  targets->reserve(args_.size());

  std::vector<TargetFilter>::const_iterator filter(filters.begin());
  std::vector<base::FilePath>::const_iterator path(args_.begin());

  for (size_t idx = 0;
       filter != filters.end() && path != args_.end();
       ++filter, ++path, ++idx) {
    const base::FilePath path_for_arg = TargetPathForArg(AbsPath(*path), idx);

    Target target(fs->GetObject(path_for_arg));
    if (target.get() == NULL) {
      Return(1, std::string("Path \"") + path_for_arg.value() +
                "\" doesn't exists");
      return false;
    }

    if (Match(target, *filter, fs))
      targets->push_back(target);
    else
      return false;
  }

  return true;
}

} // namespace command
} // namespace fileman
