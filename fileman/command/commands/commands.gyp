# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd

{
  'includes': [
    '../../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'commands',
      'type': '<(component)',
      'sources': [
        'command.cc',
        'command.h',
        'command_for_opcode.cc',
        'command_for_opcode.h',
        'common.h',
        'copy.cc',
        'copy.h',
        'change_directory.cc',
        'change_directory.h',
        'lock.cc',
        'lock.h',
        'make_object.cc',
        'make_object.h',
        'move.cc',
        'move.h',
        'print.cc',
        'print.h',
        'remove_object.cc',
        'remove_object.h',
        'unlock.cc',
        'unlock.h',
      ],
      'dependencies': [
        '../../base/base.gyp:base',
        '../../session/session.gyp:user',
        '../../vfs/vfs.gyp:vfs',
      ],
    },
  ],
}
