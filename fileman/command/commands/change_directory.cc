#include "fileman/command/commands/change_directory.h"

#include "fileman/command/commands/common.h"
#include "fileman/session/environment.h"
#include "fileman/vfs/object.h"
#include "fileman/vfs/filesystem.h"
#include "fileman/vfs/lock/lock.h"

#include "fileman/base/logging.h"

#include <sstream>

namespace fileman {
namespace command {

typedef ChangeDirectory::TargetFilter TargetFilter;

ChangeDirectory::~ChangeDirectory() {}

std::vector<TargetFilter> ChangeDirectory::TargetFilters() const {
  static TargetFilter filters[] = {
    TargetFilter
    (
     vfs::Object::kDirectory,
     Command::kRead
    ),
  };
  return std::vector<TargetFilter>(filters,
      filters + sizeof(filters) / sizeof(filters[0]));
}

base::FilePath ChangeDirectory::TargetPathForArg(const base::FilePath& arg,
                                                 size_t arg_idx) const {
  return arg; // we will changet to exactly specified target
}

void ChangeDirectory::DoExecute(vfs::Filesystem* fs,
                                const std::vector<Target>& targets) {
  DCHECK(WhoExecuteThis());
  DCHECK(Environment());
  DCHECK(targets.size() == 1);

  const vfs::Object* new_cwd = targets.front().get();
  DCHECK(new_cwd->type() == vfs::Object::kDirectory);

  Environment()->ChangeWorkDirectory(new_cwd->path());
  Return(0, new_cwd->path().value());
}

} // namespace command
} // namespace fileman
