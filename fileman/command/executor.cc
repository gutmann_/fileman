#include "fileman/command/executor.h"

#include "fileman/command/commands/command.h"

#include "fileman/message/message.h"
#include "fileman/session/user.h"
#include "fileman/vfs/filesystem.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"
#include "fileman/base/files/file_path.h"
#include "fileman/base/memory/scoped_ptr.h"

namespace fileman {
namespace command {

namespace {

std::vector<base::FilePath> MessageArgsToPaths(const message::Message& msg) {
  std::vector<base::FilePath> ret;
  ret.reserve(msg.args().size());

  for (std::vector<std::string>::const_iterator i(msg.args().begin());
       i != msg.args().end(); ++i) {
    ret.push_back(base::FilePath(*i));
  }
  return ret;
}

scoped_ptr<Command> FillCommandUsingMessage(scoped_ptr<Command> command,
                                            const message::Message& msg) {

  const std::vector<base::FilePath> args =
    MessageArgsToPaths(msg);
  for (std::vector<base::FilePath>::const_iterator i(args.begin());
       i != args.end(); ++i) {
    DVLOG(4) << "Putting \"" << i->value() << "\" to arglist";
    command->PutArg(*i);
  }

  return command.Pass();
}

} // anonimous namespace

void Executor::SetTarget(vfs::Filesystem* target) {
  DCHECK(target);
  target_ = target;
}

void Executor::OnCommandExecuted(const OnExecutedCB& cb) {
  on_executed_cb_ = cb;
}

void Executor::Execute(const message::Message& command_msg,
                       const session::User& by_user,
                       session::Environment* env) {
  DCHECK(target_);
  DCHECK(env);

  // search command by it's opcode
  scoped_ptr<Command> command = Command::ForOpcode(command_msg.opcode());
  if (command) {
    Command::OnExecutedCB on_command_executed =
      base::Bind(on_executed_cb_, command_msg);
    command->OnExecuted(on_command_executed);

    // fill command with arguments taken from |command_msg|
    command = FillCommandUsingMessage(command.Pass(), command_msg);
    if (!command) { // we can't consdtruct command from |command_msg|
      on_executed_cb_.Run(command_msg, 1, "Can't construct command");
    } else {
      command->WillExecuteBy(by_user);
      command->WillExecutedIn(env);
      std::vector<Command::Target> targets;

      if (command->GetTargets(target_, &targets))
        command->ExecuteOn(target_, targets);
      else
        on_executed_cb_.Run(command_msg,
                            1, "Can't construct command");
    }
  } else {
    on_executed_cb_.Run(command_msg, 1, "Command not found");
  }
}

} // namespace command
} // namespace fileman
