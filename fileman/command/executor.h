#ifndef COMMAND_EXECUTOR_H_
#define COMMAND_EXECUTOR_H_

#include "fileman/base/callback.h"

#include <string>

namespace fileman {

namespace message {
class Message;
}

namespace session {
class User;
class Environment;
}

namespace vfs {
class Filesystem;
}

namespace command {

/// Execute command in message in specified target filesystem
class Executor {
 public:
  typedef base::Callback<void(const message::Message& msg, int error,
                              const std::string& message)>
      OnExecutedCB;

  void SetTarget(vfs::Filesystem* target);

  /// Specify callback for listen "on executed" events.
  void OnCommandExecuted(const OnExecutedCB& cb);

  void Execute(const message::Message& command, const session::User& by_user,
               session::Environment* env);
 private:
  vfs::Filesystem* target_;

  OnExecutedCB on_executed_cb_;
};

} // namespace command
} // namespace fileman

#endif // COMMAND_EXECUTOR_H_
