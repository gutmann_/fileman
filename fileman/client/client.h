#ifndef CLIENT_CLIENT_H_
#define CLIENT_CLIENT_H_

#include "fileman/base/callback.h"

#include <string>
#include <vector>

namespace fileman {

namespace message {
class Message;
} // namespace message

namespace session {
class User;
} // namespace session

namespace client {

class ClientImpl;

class Client {
 public:
  typedef base::Callback<void(Client* client)> OnConnectCB;
  typedef base::Callback<void(const message::Message& msg)> OnMessageCB;
  typedef base::Callback<
      void(const std::vector<uint8_t>& bad_msg)> OnBadMessageCB;

  /// Create client
  /// @param host_addr ip addr (not hostname!) of server
  /// @param port number of server
  /// @param user used to auth on server
  Client(const std::string& host_addr, int port, const session::User& user);
  ~Client();

  bool Connect();
  bool Connected() const;
  bool Disconnect();

  void Send(const message::Message& msg);

  void OnConnect(const OnConnectCB& cb);
  void OnMessage(const OnMessageCB& cb);
  void OnBadMessage(const OnBadMessageCB& cb);
 private:
  void ProcessOnConnectEvent();
  void ProcessMessage(const message::Message& msg);
  bool CanSend(const message::Message& msg) const;

  std::vector<uint8_t> GetToken() const;
  uint16_t NextSequenceNumber();

  const session::User& user_;

  uint16_t current_seqnum_;

  ClientImpl* impl_;

  OnConnectCB on_connect_cb_;
  OnMessageCB on_message_cb_;
};

} // namespace client
} // namespace fileman

#endif // CLIENT_CLIENT_H_
