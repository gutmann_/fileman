# Copyright 2015 Segrey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd
#
# GYP file for RTP generation targets.

{
  'includes': [
    '../common.gypi',
  ],
  'targets': [
    {
      'target_name': 'client',
      'type': '<(component)',
      'sources': [
        'client.cc',
        'client.h',
        'line_to_message.cc',
        'line_to_message.h',
        'message_filter.h',
        'text_file_reader.cc',
        'text_file_reader.h'
      ],
      'dependencies': [
        '../session/session.gyp:user',
        '../base/base.gyp:base',
        '../third_party/libevent/libevent.gyp:libevent',
      ],
    },
  ],
}
