#ifndef CLIENT_LINE_TO_MESSAGE_H_
#define CLIENT_LINE_TO_MESSAGE_H_

#include "fileman/base/callback.h"

#include <string>

namespace fileman {

namespace message {
class Message;
} // namespace message

namespace client {

class LineToMessage {
 public:
  typedef base::Callback<void(const message::Message& msg)> OnMessageCB;
  typedef base::Callback<void(const std::string& line)>     OnParseErrorCB;

  LineToMessage();

  void Put(const std::string& line);

  void OnMessage(const OnMessageCB& cb);
  void OnParseError(const OnParseErrorCB& cb);
 private:
  OnMessageCB on_message_cb_;
  OnParseErrorCB on_parse_error_cb_;
};

} // namespace client
} // namespace fileman

#endif // CLIENT_TEXT_FILE_READER_H_
