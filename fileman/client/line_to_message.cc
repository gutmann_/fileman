#include "fileman/client/line_to_message.h"

#include "fileman/message/parser.h"
#include "fileman/message/message.h"

#include "fileman/base/bind.h"

namespace {

void DefaultOnParseErrorCallback(const std::string& line) {
  LOG(ERROR) << "Parse error: " << line;
}

} // anonimous namespace

namespace fileman {
namespace client {

LineToMessage::LineToMessage()
  : on_message_cb_(),
    on_parse_error_cb_(base::Bind(&DefaultOnParseErrorCallback)) {}

void LineToMessage::Put(const std::string& line) {
  message::Message message;
  message::Parser parser;

  if (parser.Parse(line, &message))
    on_message_cb_.Run(message);
  else
    on_parse_error_cb_.Run(line);
}

void LineToMessage::OnMessage(const OnMessageCB& cb) {
  on_message_cb_ = cb;
}

void LineToMessage::OnParseError(const OnParseErrorCB& cb) {
  on_parse_error_cb_ = cb;
}

} // namespace client
} // namespace fileman
