#ifndef CLIENT_TEXT_FILE_READER_H_
#define CLIENT_TEXT_FILE_READER_H_

#include "fileman/third_party/libevent/event.h"

#include "fileman/base/callback.h"

#include <string>

namespace fileman {
namespace client {

class TextFileReader {
 public:
  typedef base::Callback<void(const std::string& line)> OnReadLineCB;

  TextFileReader(int fd);
  ~TextFileReader();

  void OnReadLine(const OnReadLineCB& cb);
 private:
  static void OnReadEvent(int fd, short ev, void *arg);
  void ProcessReadEvent(int fd);

  event ev_;
  evbuffer* buffer_;

  OnReadLineCB on_read_line_cb_;
};

} // namespace client
} // namespace fileman

#endif // CLIENT_TEXT_FILE_READER_H_
