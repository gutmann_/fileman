#include "fileman/client/text_file_reader.h"

#include "fileman/base/logging.h"
#include "fileman/base/memory/scoped_ptr.h"

namespace {

enum { kReadChunkSize = 4096 };

} // anonimous namespace

namespace fileman {
namespace client {

TextFileReader::TextFileReader(int fd)
  : ev_(),
    buffer_(NULL),
    on_read_line_cb_() {
  buffer_ = evbuffer_new();

  event_set(&ev_, fd, EV_READ, &OnReadEvent, this);
  event_add(&ev_, 0);
}

TextFileReader::~TextFileReader() {
  if (buffer_)
    evbuffer_free(buffer_);
}

void TextFileReader::OnReadLine(const OnReadLineCB& cb) {
  on_read_line_cb_ = cb;
}

// static
void TextFileReader::OnReadEvent(int fd, short ev, void *arg) {
  DCHECK(arg);
  DCHECK_EQ(ev, EV_READ);

  TextFileReader* this_ =
    reinterpret_cast<TextFileReader*>(arg);
  this_->ProcessReadEvent(fd);
}

void TextFileReader::ProcessReadEvent(int fd) {
  int read = evbuffer_read(buffer_, fd, kReadChunkSize);
  if (read > 0) {
    scoped_ptr<char, base::FreeDeleter> line;
    for (line.reset(evbuffer_readline(buffer_));
         line != NULL; line.reset(evbuffer_readline(buffer_))) {
      on_read_line_cb_.Run(line.get());
    }
  }

  event_add(&ev_, 0);
}

} // namespace fileman
} // namespace client
