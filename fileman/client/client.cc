#include "fileman/client/client.h"

#include "fileman/message/buffer_reader.h"
#include "fileman/message/buffer_writer.h"
#include "fileman/message/message.h"
#include "fileman/message/stream_parser.h"
#include "fileman/session/user.h"

#include "fileman/base/bind.h"
#include "fileman/base/logging.h"
#include "fileman/third_party/libevent/event.h"

#include <arpa/inet.h>
#include <strings.h>
#include <unistd.h>

namespace {

enum { kReadChunkSize = 4096 };

void DefaultOnBadMessageCallback(const std::vector<uint8_t>& bad_msg) {
  LOG(ERROR) << "Got bad message from server (" << bad_msg.size() << " bytes)";
}

} // anonimous namespace

namespace fileman {
namespace client {

typedef Client::OnMessageCB OnMessageCB;
typedef Client::OnBadMessageCB OnBadMessageCB;

class ClientImpl {
 public:
  typedef base::Callback<void()> OnConnectCB;

  ClientImpl(const std::string& host_addr, int port);
  ~ClientImpl();

  bool Connect();
  bool Connected() const;
  bool Disconnect();

  void Send(const message::Message& msg);

  void OnConnect(const OnConnectCB& cb);
  void OnMessage(const OnMessageCB& cb);
  void OnBadMessage(const OnBadMessageCB& cb);
 private:
  bool InitEventHandling();
  void FreeEventHandling();

  static void OnReadCB(bufferevent* bevent, void* arg);
  static void OnWriteCB(bufferevent* bevent, void* arg);
  static void OnErrorCB(bufferevent* bevent, short what, void* arg);

  void ProcessReadEvent(bufferevent* bevent);
  void ProcessWriteEvent(bufferevent* bevent);
  void ProcessError(bufferevent* bevent, short what);
  void ProcessStreamParseError(message::Message::Status parse_status,
                               const std::vector<uint8_t>& msg);

  const std::string host_addr_;
  const int port_;

  int sock_fd_;
  sockaddr_in serveraddr_;

  bufferevent* sock_bufevent_;

  message::StreamParser stream_parser_;

  OnConnectCB on_connect_cb_;
  OnBadMessageCB on_bad_message_cb_;
};

ClientImpl::ClientImpl(const std::string& host_addr, int port)
  : host_addr_(host_addr),
    port_(port),
    sock_fd_(0),
    serveraddr_(),
    sock_bufevent_(NULL),
    stream_parser_(),
    on_bad_message_cb_(base::Bind(&DefaultOnBadMessageCallback)) {
  bzero(&serveraddr_, sizeof(serveraddr_));

  stream_parser_.OnStreamParseError(
      base::Bind(&ClientImpl::ProcessStreamParseError,
                 base::Unretained(this)));
}

ClientImpl::~ClientImpl() {
  if (Connected())
    Disconnect();
}

bool ClientImpl::Connect() {
  DCHECK(!Connected());

  LOG(INFO) << "Connecting to " << host_addr_ << ":" << port_;
  sock_fd_ = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd_ < 0) {
    LOG(ERROR) << "Can't create socket";
    return false;
  }

  serveraddr_.sin_family = AF_INET;
  serveraddr_.sin_addr.s_addr = inet_addr(host_addr_.c_str());
  serveraddr_.sin_port = htons(port_);

  int connect_status = connect(sock_fd_, (const sockaddr*)&serveraddr_,
                               sizeof(serveraddr_));
  if (connect_status < 0) {
    LOG(ERROR) << "Can't connect to server";
    return false;
  }

  LOG(INFO) << "Connected to " << host_addr_ << ":" << port_;

  return InitEventHandling();
}

bool ClientImpl::Connected() const { return sock_fd_ != 0; }

bool ClientImpl::Disconnect() {
  DCHECK(Connected());

  LOG(INFO) << "Disconnecting from " << host_addr_ << ":" << port_;
  FreeEventHandling();
  
  const int close_status = close(sock_fd_);
  if (close_status != 0) {
    LOG(ERROR) << "Error while closing socket";
  } else {
    LOG(INFO) << "Successfully disconnected from " 
              << host_addr_ << ":" << port_;
  }

  return close_status;
}

void ClientImpl::Send(const message::Message& msg) {
  message::BufferWriter writer;
  msg.WriteTo(&writer);
  DVLOG(3) << "Sending message \"" << msg.AsString() << '\"';
  bufferevent_write(sock_bufevent_, writer.Buffer(), writer.Size());
}

void ClientImpl::OnConnect(const OnConnectCB& cb) {
  on_connect_cb_ = cb;
}

void ClientImpl::OnMessage(const OnMessageCB& cb) {
  stream_parser_.OnMessage(cb);
}

void ClientImpl::OnBadMessage(const OnBadMessageCB& cb) {
  on_bad_message_cb_ = cb;
}

bool ClientImpl::InitEventHandling() {
  DCHECK(Connected());
  DCHECK(sock_bufevent_ == NULL);

  DVLOG(3) << "Initializing event handling...";
  sock_bufevent_ =
    bufferevent_new(sock_fd_,
                    &OnReadCB,
                    &OnWriteCB,
                    &OnErrorCB,
                    this);

  if (sock_bufevent_ == NULL) {
    LOG(ERROR) << "Can't create bufferevent";
    return false;
  }

  if (bufferevent_enable(sock_bufevent_, EV_READ | EV_WRITE) != 0) {
    LOG(ERROR) << "Can't enable eventuffer";
    return false;
  }

  DVLOG(3) << "Event handling successfully initialized";
  return true;
}

void ClientImpl::FreeEventHandling() {
  if (sock_bufevent_) {
    if (bufferevent_disable(sock_bufevent_, EV_READ | EV_WRITE) != 0) {
      LOG(ERROR) << "Can't disable eventbuffer";
    }
    bufferevent_free(sock_bufevent_);
  }
}

// static
void ClientImpl::OnReadCB(bufferevent* bevent, void* arg) {
  DCHECK(arg);
  ClientImpl* this_ = reinterpret_cast<ClientImpl*>(arg);
  this_->ProcessReadEvent(bevent);
}

// static
void ClientImpl::OnWriteCB(bufferevent* bevent, void* arg) {
  DCHECK(arg);
  ClientImpl* this_ = reinterpret_cast<ClientImpl*>(arg);
  this_->ProcessWriteEvent(bevent);
}

// static
void ClientImpl::OnErrorCB(bufferevent* bevent, short what, void* arg) {
  DCHECK(arg);
  ClientImpl* this_ = reinterpret_cast<ClientImpl*>(arg);
  this_->ProcessError(bevent, what);
}

void ClientImpl::ProcessReadEvent(bufferevent* bevent) {
  DCHECK_EQ(sock_bufevent_, bevent);

  while (true) {
    uint8_t data[kReadChunkSize];
    size_t read = bufferevent_read(bevent,  data, sizeof(data));
    if (read > 0)
      stream_parser_.Push(data, read);
    else
      break;
  }
}

void ClientImpl::ProcessWriteEvent(bufferevent* bevent) {
  DCHECK_EQ(sock_bufevent_, bevent);

  // removing `on read` callback
  bufferevent_setcb(sock_bufevent_,
                    &OnReadCB,
                    NULL,
                    &OnErrorCB,
                    this);

  on_connect_cb_.Run();
}

void ClientImpl::ProcessError(bufferevent* bevent, short what) {
  LOG(ERROR) << "Error while interacting with buffer event "
             << "(code #" << what << ")";
}

void ClientImpl::ProcessStreamParseError(message::Message::Status parse_status,
                                         const std::vector<uint8_t>& msg) {
  on_bad_message_cb_.Run(msg);
}

namespace {

void DefaultOnConnectCallback(Client* client) {
  LOG(WARNING) << "You not set `OnConnect` callback";
}

} // anonimous namespace


Client::Client(const std::string& host_addr, int port,
               const session::User& user)
  : user_(user),
    current_seqnum_(0),
    impl_(new ClientImpl(host_addr, port)),
    on_connect_cb_(base::Bind(&DefaultOnConnectCallback)),
    on_message_cb_() {
  impl_->OnConnect(base::Bind(&Client::ProcessOnConnectEvent,
                              base::Unretained(this)));
  impl_->OnMessage(base::Bind(&Client::ProcessMessage,
                              base::Unretained(this)));
}

Client::~Client() {
  if (impl_)
    delete impl_;
}

bool Client::Connect() {
  DCHECK(impl_);
  LOG(INFO) << "Using \"" << user_.name() << "\" as name of user";
  return impl_->Connect();
}

bool Client::Connected() const {
  DCHECK(impl_);
  return impl_->Connected();
}

bool Client::Disconnect() {
  DCHECK(impl_);
  return impl_->Disconnect();
}

void Client::Send(const message::Message& msg) {
  DCHECK(impl_);

  if (CanSend(msg)) {
    message::Message to_send(msg);
    to_send.SetToken(GetToken());
    to_send.SetSequenceNumber(NextSequenceNumber());

    impl_->Send(to_send);
  }
}

void Client::OnConnect(const OnConnectCB& cb) {
  on_connect_cb_ = cb;
}

void Client::OnMessage(const OnMessageCB& cb) {
  on_message_cb_ = cb;
}

void Client::OnBadMessage(const OnBadMessageCB& cb) {
  DCHECK(impl_);
  impl_->OnBadMessage(cb);
}

void Client::ProcessOnConnectEvent() {
  on_connect_cb_.Run(this);
}

void Client::ProcessMessage(const message::Message& msg) {
  on_message_cb_.Run(msg);
}

bool Client::CanSend(const message::Message& msg) const {
  return true;
}

std::vector<uint8_t> Client::GetToken() const {
  const std::string& username = user_.name();
  std::vector<uint8_t> token(username.size());
  std::copy(username.data(), username.data() + username.size(),
            token.begin());
  return token;
}

uint16_t Client::NextSequenceNumber() {
  return current_seqnum_++;
}

} // namespace client
} // namespace fileman
