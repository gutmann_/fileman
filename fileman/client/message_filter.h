#ifndef CLIENT_MESSAGE_FILTER_H_
#define CLIENT_MESSAGE_FILTER_H_

#include "fileman/base/bind.h"
#include "fileman/base/callback.h"

namespace fileman {

namespace message {
class Message;
} // namespace message

namespace client {

namespace {
void DefaultOnMatchCallback(const message::Message& msg) {}
} // anonimous namespace

template <class UnaryPredicate>
class MessageFilter {
 public:
  typedef base::Callback<void(const message::Message& msg)> OnMatchFilterCB;
  typedef base::Callback<void(const message::Message& msg)> OnMessageCB;

  MessageFilter() {
    OnMatch(base::Bind(&DefaultOnMatchCallback));
  }

  void Put(const message::Message& message) {
    if (predicate_(message))
      on_match_filter_cb_.Run(message);
    else
      on_message_cb_.Run(message);
  }

  void OnMatch(const OnMatchFilterCB& cb) { on_match_filter_cb_ = cb; }
  void OnMessage(const OnMessageCB& cb) { on_message_cb_ = cb; }
 private:
  UnaryPredicate predicate_;

  OnMatchFilterCB on_match_filter_cb_;
  OnMessageCB on_message_cb_;
};

} // namespace client
} // namespace fileman

#endif // CLIENT_MESSAGE_FILTER_H_
