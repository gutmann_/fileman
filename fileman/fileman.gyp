# Copyright 2015 Sergey Guzhov. All rights reserved.
#
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file or at
# https://developers.google.com/open-source/licenses/bsd

{
  'includes': [
    'common.gypi',
  ],
  'targets': [
    {
      'target_name': 'server',
      'type': 'executable',
      'sources': [
        'app/fileman.cc',
      ],
      'dependencies': [
        'base/base.gyp:base',
        'command/command.gyp:command',
        'session/session.gyp:session',
        'session/server/server.gyp:libserver',
        'vfs/vfs.gyp:vfs',
        'third_party/libevent/libevent.gyp:libevent',
      ],
    },

    {
      'target_name': 'connect',
      'type': 'executable',
      'sources': [
        'app/fileman_client.cc',
      ],
      'dependencies': [
        'base/base.gyp:base',
        'message/message.gyp:message',
        'client/client.gyp:client',
        'third_party/libevent/libevent.gyp:libevent',
      ],
    },

    {
      'target_name': 'All',
      'type': 'none',
      'dependencies': [
      ],
    },
  ],
}
