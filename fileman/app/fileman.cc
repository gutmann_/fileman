#include "fileman/base/command_line.h"
#include "fileman/base/logging.h"
#include "fileman/base/files/file_path.h"
#include "fileman/command/executor.h"
#include "fileman/session/session_manager.h"
#include "fileman/session/server/server.h"
#include "fileman/vfs/filesystem.h"

#include "fileman/base/files/file_path.h"
#include "fileman/base/files/file_util.h"
#include "fileman/base/json/json_reader.h"
#include "fileman/base/values.h"

#include "fileman/third_party/libevent/event.h"

#include <stdlib.h>
#include <unistd.h>

#include <fstream>
#include <iostream>

#define DEFAULT_PORT (9999)

namespace fileman {
namespace {

bool ParseParams(const CommandLine::StringVector& args,
                 base::FilePath* path_to_config) {
  if (args.size() != 1) return false;
  *path_to_config = base::FilePath(args.front());
  return true;
}

void PrintUsage(const CommandLine* command_line) {
  std::cerr << "Usage: " << command_line->GetProgram().BaseName().value()
            << " CONFIG_FILE" <<
  std::endl;
}

struct Config {
  std::string iface;
  int port;
};

bool ParseConfig(const std::string& config, Config* into) {
  using base::DictionaryValue;
  using base::JSONReader;
  using base::Value;

  scoped_ptr<Value> json(JSONReader::Read(config));
  DictionaryValue* dict;

  if (!json->GetAsDictionary(&dict)) {
    std::cerr << "Can't parse json" << std::endl;
    return false;
  }

  return
    dict->GetString("server.listen.iface", &into->iface) &&
    dict->GetInteger("server.listen.port", &into->port);
}

bool ReadConfig(const CommandLine* command_line, Config* into) {
  base::FilePath path_to_config;
  if (!ParseParams(command_line->GetArgs(), &path_to_config)) {
    PrintUsage(command_line);
    return false;
  }

  std::string file_contents;
  if (!ReadFileToString(path_to_config, &file_contents)) {
    std::cerr << "Can't read config file" << std::endl;
    return false;
  }
  
  if (!ParseConfig(file_contents, into)) {
    std::cerr << "Error while parsing config file" << std::endl;
    return false;
  }

  return true;
}

using session::Server;
using session::SessionManager;

int Main(CommandLine* command_line) {
  Config config;
  if (!ReadConfig(command_line, &config))
    return 1;

  event_base *base = event_init();

  Server server(config.iface, config.port);

  const bool server_is_ready =
    server.Init() &&
    server.Bind() &&
    server.Listen();
  
  if (!server_is_ready) {
    LOG(ERROR) << "Server not ready for connection processing! Exiting...";
    return 1;
  }

  vfs::Filesystem filesystem;
  command::Executor command_executor;
  command_executor.SetTarget(&filesystem);

  SessionManager sessions;
  sessions.ExecuteCommandsUsing(&command_executor);
  sessions.ListenMessagesFrom(&server);

  event_dispatch();
  event_base_free(base);
  return 0;
}

} // anonimous namespace
} // namespace fileman

int main(int argc, char* argv[]) {
  CommandLine::Init(argc, argv);
  logging::LoggingSettings logging_settings;
  logging::InitLogging(logging_settings);

  return fileman::Main(CommandLine::ForCurrentProcess());
}
