#include "fileman/base/bind.h"
#include "fileman/base/command_line.h"
#include "fileman/base/logging.h"
#include "fileman/base/files/file_path.h"
#include "fileman/base/strings/string_number_conversions.h"
#include "fileman/base/strings/string_tokenizer.h"
#include "fileman/client/client.h"
#include "fileman/client/line_to_message.h"
#include "fileman/client/message_filter.h"
#include "fileman/client/text_file_reader.h"
#include "fileman/message/message.h"
#include "fileman/session/user.h"

#include "fileman/base/strings/string_util.h"

#include "fileman/third_party/libevent/event.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>

#define DEFAULT_PORT (9999)
#define PROGRAM_NAME (CommandLine::ForCurrentProcess() \
                      ->GetProgram().BaseName().value())

namespace fileman {
namespace client {
namespace {

event_base* EVENT_BASE = NULL;

std::string FirstWordOf(const std::string& s) {
  base::StringTokenizer tokenizer(s, " \t");
  if (tokenizer.GetNext()) return tokenizer.token();
  return "";
}

struct FilterQuitMessage {
  bool operator ()(const message::Message& message) const {
    return message.opcode() == message::QUIT;
  }
};

void PrintPS1() {
  std::cout << "$ " << std::flush;
}

void DoQuit() {
  event_base_loopbreak(EVENT_BASE);
}

void Quit(const message::Message& msg) {
  DCHECK(msg.opcode() == message::QUIT);
  DCHECK(EVENT_BASE);

  DoQuit();
}

void OnConnect(Client* to) {
  DCHECK(to);
  to->Send(message::Message(message::AUTH));
}

void OnMessage(const message::Message& msg) {
  std::string msg_text;
  base::TrimString(msg.ArgsAsString(), "\n \t", &msg_text);

  if (msg.opcode() == message::NOTIFY)
    std::cout << '\r' << "server say: ";
  if (!msg_text.empty()) {
    if (msg.opcode() != message::NOTIFY) std::cout << '\r';
    std::cout << msg_text << std::endl;
  }
  PrintPS1();
}

void OnBadMessage(const std::vector<uint8_t>& bad_msg) {
  std::cerr << PROGRAM_NAME   << ": malformed response from remote host ("
            << bad_msg.size() << " bytes length)" <<
  std::endl;
  PrintPS1();
}

void OnCommandParseError(const std::string& line) {
  if (!line.empty()) {
    std::cerr << PROGRAM_NAME << ": " << FirstWordOf(line)
              << ": command not found" <<
    std::endl;
  }
  PrintPS1();
}

void PrintUsage(const CommandLine* command_line) {
  std::cerr << "Usage: " << command_line->GetProgram().BaseName().value()
            << " server_name[:port] UserName" <<
  std::endl;
}

bool ParseParams(const CommandLine::StringVector& args,
                 std::string* host,
                 int* port,
                 std::string* user) {
  if (args.size() != 2) return false;

  CommandLine::StringType host_and_port = args.at(0);
  base::StringTokenizer tokenizer(host_and_port, ":");
  if (tokenizer.GetNext()) *host = tokenizer.token();
  if (tokenizer.GetNext()) {
    std::string potr_str = tokenizer.token();
    int portno;
    if (!base::StringToInt(potr_str, &portno)) return false;
    if (portno <= 0) return false;
    *port = portno;
  }

  *user = args.at(1);
  return true;
}

bool HostnameToIp(const std::string& hostname, std::string* ip) {
  hostent *he;
  in_addr **addr_list;

  if ((he = gethostbyname(hostname.c_str())) == NULL)
    return false;

  addr_list = (in_addr **)he->h_addr_list;
  for (int i = 0; addr_list[i] != NULL; ++i) {
    *ip = inet_ntoa(*addr_list[i]);
    return true;
  }
  return false;
}

int Main(CommandLine* command_line) {
  std::string host;
  int port = DEFAULT_PORT;
  std::string username;
  if (!ParseParams(command_line->GetArgs(), &host, &port, &username)) {
    PrintUsage(command_line);
    return 1;
  }

  std::string server_ip;
  if (!HostnameToIp(host, &server_ip)) {
    std::cerr << "Unknown host: \"" << host << '\"' << std::endl;
    return 1;
  }

  EVENT_BASE = event_init();

  session::User user(username);

  typedef MessageFilter<FilterQuitMessage> QuitFilter;

  TextFileReader stdin_reader(STDIN_FILENO);

  QuitFilter quit_filter;
  quit_filter.OnMatch(base::Bind(&Quit));
  
  LineToMessage line_to_message;
  line_to_message.OnParseError(base::Bind(&OnCommandParseError));
  
  Client cli(server_ip, port, user);
  cli.OnConnect(base::Bind(&OnConnect));
  cli.OnMessage(base::Bind(&OnMessage));
  cli.OnBadMessage(base::Bind(&OnBadMessage));

  // Code below creates pipe in the manner of UNIX pipes:
  //  stdin | line_to_message | quit_filter | client.Send
  stdin_reader.OnReadLine(base::Bind(&LineToMessage::Put,
                                     base::Unretained(&line_to_message)));
  line_to_message.OnMessage(base::Bind(&QuitFilter::Put,
                                       base::Unretained(&quit_filter)));
  quit_filter.OnMessage(base::Bind(&Client::Send,
                                   base::Unretained(&cli)));

  if (!cli.Connect()) return 1;
  PrintPS1();

  event_dispatch();
  event_base_free(EVENT_BASE);

  return 0;
}

} // anonimous namespace
} // namespace client
} // namespace fileman

int main(int argc, char* argv[]) {
  CommandLine::Init(argc, argv);
  logging::LoggingSettings logging_settings;
  logging::InitLogging(logging_settings);

  return fileman::client::Main(CommandLine::ForCurrentProcess());
}
